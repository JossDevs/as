@extends('layouts.admin')
@section('content')
<div class="content">

    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    {{ trans('global.edit') }} {{ trans('cruds.marca.title_singular') }}
                </div>
                <div class="panel-body">
                    <form method="POST" action="{{ route("admin.marcas.update", [$marca->id]) }}" enctype="multipart/form-data">
                        @method('PUT')
                        @csrf
                        <div class="form-group {{ $errors->has('nombre') ? 'has-error' : '' }}">
                            <label class="required" for="nombre">{{ trans('cruds.marca.fields.nombre') }}</label>
                            <input class="form-control" type="text" name="nombre" id="nombre" value="{{ old('nombre', $marca->nombre) }}" required>
                            @if($errors->has('nombre'))
                                <span class="help-block" role="alert">{{ $errors->first('nombre') }}</span>
                            @endif
                            <span class="help-block">{{ trans('cruds.marca.fields.nombre_helper') }}</span>
                        </div>
                        <div class="form-group {{ $errors->has('correo_electronico') ? 'has-error' : '' }}">
                            <label class="required" for="correo_electronico">{{ trans('cruds.marca.fields.correo_electronico') }}</label>
                            <input class="form-control" type="email" name="correo_electronico" id="correo_electronico" value="{{ old('correo_electronico', $marca->correo_electronico) }}" required>
                            @if($errors->has('correo_electronico'))
                                <span class="help-block" role="alert">{{ $errors->first('correo_electronico') }}</span>
                            @endif
                            <span class="help-block">{{ trans('cruds.marca.fields.correo_electronico_helper') }}</span>
                        </div>
                        <div class="form-group {{ $errors->has('telefono') ? 'has-error' : '' }}">
                            <label class="required" for="telefono">{{ trans('cruds.marca.fields.telefono') }}</label>
                            <input class="form-control" type="text" name="telefono" id="telefono" value="{{ old('telefono', $marca->telefono) }}" required>
                            @if($errors->has('telefono'))
                                <span class="help-block" role="alert">{{ $errors->first('telefono') }}</span>
                            @endif
                            <span class="help-block">{{ trans('cruds.marca.fields.telefono_helper') }}</span>
                        </div>
                        <div class="form-group">
                            <button class="btn btn-danger" type="submit">
                                {{ trans('global.save') }}
                            </button>
                        </div>
                    </form>
                </div>
            </div>



        </div>
    </div>
</div>
@endsection