@extends('layouts.admin')
@section('content')
<div class="content">

    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    {{ trans('global.show') }} {{ trans('cruds.marca.title') }}
                </div>
                <div class="panel-body">
                    <div class="form-group">
                        <div class="form-group">
                            <a class="btn btn-default" href="{{ route('admin.marcas.index') }}">
                                {{ trans('global.back_to_list') }}
                            </a>
                        </div>
                        <table class="table table-bordered table-striped">
                            <tbody>
                                <tr>
                                    <th>
                                        {{ trans('cruds.marca.fields.id') }}
                                    </th>
                                    <td>
                                        {{ $marca->id }}
                                    </td>
                                </tr>
                                <tr>
                                    <th>
                                        {{ trans('cruds.marca.fields.nombre') }}
                                    </th>
                                    <td>
                                        {{ $marca->nombre }}
                                    </td>
                                </tr>
                                <tr>
                                    <th>
                                        {{ trans('cruds.marca.fields.correo_electronico') }}
                                    </th>
                                    <td>
                                        {{ $marca->correo_electronico }}
                                    </td>
                                </tr>
                                <tr>
                                    <th>
                                        {{ trans('cruds.marca.fields.telefono') }}
                                    </th>
                                    <td>
                                        {{ $marca->telefono }}
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                        <div class="form-group">
                            <a class="btn btn-default" href="{{ route('admin.marcas.index') }}">
                                {{ trans('global.back_to_list') }}
                            </a>
                        </div>
                    </div>
                </div>
            </div>

            <div class="panel panel-default">
                <div class="panel-heading">
                    {{ trans('global.relatedData') }}
                </div>
                <ul class="nav nav-tabs" role="tablist" id="relationship-tabs">
                    <li role="presentation">
                        <a href="#marca_medicamentos" aria-controls="marca_medicamentos" role="tab" data-toggle="tab">
                            {{ trans('cruds.medicamento.title') }}
                        </a>
                    </li>
                </ul>
                <div class="tab-content">
                    <div class="tab-pane" role="tabpanel" id="marca_medicamentos">
                        @includeIf('admin.marcas.relationships.marcaMedicamentos', ['medicamentos' => $marca->marcaMedicamentos])
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>
@endsection