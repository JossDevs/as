@extends('layouts.admin')
@section('content')
<div class="content">

    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    {{ trans('global.show') }} {{ trans('cruds.medicamento.title') }}
                </div>
                <div class="panel-body">
                    <div class="form-group">
                        <div class="form-group">
                            <a class="btn btn-default" href="{{ route('admin.medicamentos.index') }}">
                                {{ trans('global.back_to_list') }}
                            </a>
                        </div>
                        <table class="table table-bordered table-striped">
                            <tbody>
                                <tr>
                                    <th>
                                        {{ trans('cruds.medicamento.fields.id') }}
                                    </th>
                                    <td>
                                        {{ $medicamento->id }}
                                    </td>
                                </tr>
                                <tr>
                                    <th>
                                        {{ trans('cruds.medicamento.fields.categoria') }}
                                    </th>
                                    <td>
                                        {{ $medicamento->categoria->nombre ?? '' }}
                                    </td>
                                </tr>
                                <tr>
                                    <th>
                                        {{ trans('cruds.medicamento.fields.marca') }}
                                    </th>
                                    <td>
                                        {{ $medicamento->marca->nombre ?? '' }}
                                    </td>
                                </tr>
                                <tr>
                                    <th>
                                        {{ trans('cruds.medicamento.fields.nombre') }}
                                    </th>
                                    <td>
                                        {{ $medicamento->nombre }}
                                    </td>
                                </tr>
                                <tr>
                                    <th>
                                        {{ trans('cruds.medicamento.fields.precio_de_venta') }}
                                    </th>
                                    <td>
                                        {{ $medicamento->precio_de_venta }}
                                    </td>
                                </tr>
                                <tr>
                                    <th>
                                        {{ trans('cruds.medicamento.fields.precio_de_compra') }}
                                    </th>
                                    <td>
                                        {{ $medicamento->precio_de_compra }}
                                    </td>
                                </tr>
                                <tr>
                                    <th>
                                        {{ trans('cruds.medicamento.fields.composicion_de_sal') }}
                                    </th>
                                    <td>
                                        {{ $medicamento->composicion_de_sal }}
                                    </td>
                                </tr>
                                <tr>
                                    <th>
                                        {{ trans('cruds.medicamento.fields.descripcion') }}
                                    </th>
                                    <td>
                                        {{ $medicamento->descripcion }}
                                    </td>
                                </tr>
                                <tr>
                                    <th>
                                        {{ trans('cruds.medicamento.fields.efectos_colaterales') }}
                                    </th>
                                    <td>
                                        {{ $medicamento->efectos_colaterales }}
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                        <div class="form-group">
                            <a class="btn btn-default" href="{{ route('admin.medicamentos.index') }}">
                                {{ trans('global.back_to_list') }}
                            </a>
                        </div>
                    </div>
                </div>
            </div>



        </div>
    </div>
</div>
@endsection