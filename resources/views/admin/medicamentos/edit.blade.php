@extends('layouts.admin')
@section('content')
<div class="content">

    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    {{ trans('global.edit') }} {{ trans('cruds.medicamento.title_singular') }}
                </div>
                <div class="panel-body">
                    <form method="POST" action="{{ route("admin.medicamentos.update", [$medicamento->id]) }}" enctype="multipart/form-data">
                        @method('PUT')
                        @csrf
                        <div class="form-group {{ $errors->has('categoria') ? 'has-error' : '' }}">
                            <label class="required" for="categoria_id">{{ trans('cruds.medicamento.fields.categoria') }}</label>
                            <select class="form-control select2" name="categoria_id" id="categoria_id" required>
                                @foreach($categorias as $id => $entry)
                                    <option value="{{ $id }}" {{ (old('categoria_id') ? old('categoria_id') : $medicamento->categoria->id ?? '') == $id ? 'selected' : '' }}>{{ $entry }}</option>
                                @endforeach
                            </select>
                            @if($errors->has('categoria'))
                                <span class="help-block" role="alert">{{ $errors->first('categoria') }}</span>
                            @endif
                            <span class="help-block">{{ trans('cruds.medicamento.fields.categoria_helper') }}</span>
                        </div>
                        <div class="form-group {{ $errors->has('marca') ? 'has-error' : '' }}">
                            <label class="required" for="marca_id">{{ trans('cruds.medicamento.fields.marca') }}</label>
                            <select class="form-control select2" name="marca_id" id="marca_id" required>
                                @foreach($marcas as $id => $entry)
                                    <option value="{{ $id }}" {{ (old('marca_id') ? old('marca_id') : $medicamento->marca->id ?? '') == $id ? 'selected' : '' }}>{{ $entry }}</option>
                                @endforeach
                            </select>
                            @if($errors->has('marca'))
                                <span class="help-block" role="alert">{{ $errors->first('marca') }}</span>
                            @endif
                            <span class="help-block">{{ trans('cruds.medicamento.fields.marca_helper') }}</span>
                        </div>
                        <div class="form-group {{ $errors->has('nombre') ? 'has-error' : '' }}">
                            <label class="required" for="nombre">{{ trans('cruds.medicamento.fields.nombre') }}</label>
                            <input class="form-control" type="text" name="nombre" id="nombre" value="{{ old('nombre', $medicamento->nombre) }}" required>
                            @if($errors->has('nombre'))
                                <span class="help-block" role="alert">{{ $errors->first('nombre') }}</span>
                            @endif
                            <span class="help-block">{{ trans('cruds.medicamento.fields.nombre_helper') }}</span>
                        </div>
                        <div class="form-group {{ $errors->has('precio_de_venta') ? 'has-error' : '' }}">
                            <label class="required" for="precio_de_venta">{{ trans('cruds.medicamento.fields.precio_de_venta') }}</label>
                            <input class="form-control" type="number" name="precio_de_venta" id="precio_de_venta" value="{{ old('precio_de_venta', $medicamento->precio_de_venta) }}" step="0.01" required>
                            @if($errors->has('precio_de_venta'))
                                <span class="help-block" role="alert">{{ $errors->first('precio_de_venta') }}</span>
                            @endif
                            <span class="help-block">{{ trans('cruds.medicamento.fields.precio_de_venta_helper') }}</span>
                        </div>
                        <div class="form-group {{ $errors->has('precio_de_compra') ? 'has-error' : '' }}">
                            <label class="required" for="precio_de_compra">{{ trans('cruds.medicamento.fields.precio_de_compra') }}</label>
                            <input class="form-control" type="number" name="precio_de_compra" id="precio_de_compra" value="{{ old('precio_de_compra', $medicamento->precio_de_compra) }}" step="0.01" required>
                            @if($errors->has('precio_de_compra'))
                                <span class="help-block" role="alert">{{ $errors->first('precio_de_compra') }}</span>
                            @endif
                            <span class="help-block">{{ trans('cruds.medicamento.fields.precio_de_compra_helper') }}</span>
                        </div>
                        <div class="form-group {{ $errors->has('composicion_de_sal') ? 'has-error' : '' }}">
                            <label class="required" for="composicion_de_sal">{{ trans('cruds.medicamento.fields.composicion_de_sal') }}</label>
                            <input class="form-control" type="text" name="composicion_de_sal" id="composicion_de_sal" value="{{ old('composicion_de_sal', $medicamento->composicion_de_sal) }}" required>
                            @if($errors->has('composicion_de_sal'))
                                <span class="help-block" role="alert">{{ $errors->first('composicion_de_sal') }}</span>
                            @endif
                            <span class="help-block">{{ trans('cruds.medicamento.fields.composicion_de_sal_helper') }}</span>
                        </div>
                        <div class="form-group {{ $errors->has('descripcion') ? 'has-error' : '' }}">
                            <label for="descripcion">{{ trans('cruds.medicamento.fields.descripcion') }}</label>
                            <textarea class="form-control" name="descripcion" id="descripcion">{{ old('descripcion', $medicamento->descripcion) }}</textarea>
                            @if($errors->has('descripcion'))
                                <span class="help-block" role="alert">{{ $errors->first('descripcion') }}</span>
                            @endif
                            <span class="help-block">{{ trans('cruds.medicamento.fields.descripcion_helper') }}</span>
                        </div>
                        <div class="form-group {{ $errors->has('efectos_colaterales') ? 'has-error' : '' }}">
                            <label for="efectos_colaterales">{{ trans('cruds.medicamento.fields.efectos_colaterales') }}</label>
                            <textarea class="form-control" name="efectos_colaterales" id="efectos_colaterales">{{ old('efectos_colaterales', $medicamento->efectos_colaterales) }}</textarea>
                            @if($errors->has('efectos_colaterales'))
                                <span class="help-block" role="alert">{{ $errors->first('efectos_colaterales') }}</span>
                            @endif
                            <span class="help-block">{{ trans('cruds.medicamento.fields.efectos_colaterales_helper') }}</span>
                        </div>
                        <div class="form-group">
                            <button class="btn btn-danger" type="submit">
                                {{ trans('global.save') }}
                            </button>
                        </div>
                    </form>
                </div>
            </div>



        </div>
    </div>
</div>
@endsection