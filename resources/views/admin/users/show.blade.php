@extends('layouts.admin')
@section('content')
<div class="content">

    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    {{ trans('global.show') }} {{ trans('cruds.user.title') }}
                </div>
                <div class="panel-body">
                    <div class="form-group">
                        <div class="form-group">
                            <a class="btn btn-default" href="{{ route('admin.users.index') }}">
                                {{ trans('global.back_to_list') }}
                            </a>
                        </div>
                        <table class="table table-bordered table-striped">
                            <tbody>
                                <tr>
                                    <th>
                                        {{ trans('cruds.user.fields.id') }}
                                    </th>
                                    <td>
                                        {{ $user->id }}
                                    </td>
                                </tr>
                                <tr>
                                    <th>
                                        {{ trans('cruds.user.fields.name') }}
                                    </th>
                                    <td>
                                        {{ $user->name }}
                                    </td>
                                </tr>
                                <tr>
                                    <th>
                                        {{ trans('cruds.user.fields.email') }}
                                    </th>
                                    <td>
                                        {{ $user->email }}
                                    </td>
                                </tr>
                                <tr>
                                    <th>
                                        {{ trans('cruds.user.fields.email_verified_at') }}
                                    </th>
                                    <td>
                                        {{ $user->email_verified_at }}
                                    </td>
                                </tr>
                                <tr>
                                    <th>
                                        {{ trans('cruds.user.fields.roles') }}
                                    </th>
                                    <td>
                                        @foreach($user->roles as $key => $roles)
                                            <span class="label label-info">{{ $roles->title }}</span>
                                        @endforeach
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                        <div class="form-group">
                            <a class="btn btn-default" href="{{ route('admin.users.index') }}">
                                {{ trans('global.back_to_list') }}
                            </a>
                        </div>
                    </div>
                </div>
            </div>

            <div class="panel panel-default">
                <div class="panel-heading">
                    {{ trans('global.relatedData') }}
                </div>
                <ul class="nav nav-tabs" role="tablist" id="relationship-tabs">
                    <li role="presentation">
                        <a href="#usuario_medicos" aria-controls="usuario_medicos" role="tab" data-toggle="tab">
                            {{ trans('cruds.medico.title') }}
                        </a>
                    </li>
                    <li role="presentation">
                        <a href="#usuario_pacientes" aria-controls="usuario_pacientes" role="tab" data-toggle="tab">
                            {{ trans('cruds.paciente.title') }}
                        </a>
                    </li>
                    <li role="presentation">
                        <a href="#usuario_enfermeras" aria-controls="usuario_enfermeras" role="tab" data-toggle="tab">
                            {{ trans('cruds.enfermera.title') }}
                        </a>
                    </li>
                    <li role="presentation">
                        <a href="#paciente_cita" aria-controls="paciente_cita" role="tab" data-toggle="tab">
                            {{ trans('cruds.citum.title') }}
                        </a>
                    </li>
                </ul>
                <div class="tab-content">
                    <div class="tab-pane" role="tabpanel" id="usuario_medicos">
                        @includeIf('admin.users.relationships.usuarioMedicos', ['medicos' => $user->usuarioMedicos])
                    </div>
                    <div class="tab-pane" role="tabpanel" id="usuario_pacientes">
                        @includeIf('admin.users.relationships.usuarioPacientes', ['pacientes' => $user->usuarioPacientes])
                    </div>
                    <div class="tab-pane" role="tabpanel" id="usuario_enfermeras">
                        @includeIf('admin.users.relationships.usuarioEnfermeras', ['enfermeras' => $user->usuarioEnfermeras])
                    </div>
                    <div class="tab-pane" role="tabpanel" id="paciente_cita">
                        @includeIf('admin.users.relationships.pacienteCita', ['cita' => $user->pacienteCita])
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>
@endsection