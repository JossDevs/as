@extends('layouts.admin')
@section('content')
<div class="content">

    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    {{ trans('global.create') }} {{ trans('cruds.citum.title_singular') }}
                </div>
                <div class="panel-body">
                    <form method="POST" action="{{ route("admin.cita.store") }}" enctype="multipart/form-data">
                        @csrf
                        <div class="form-group {{ $errors->has('paciente') ? 'has-error' : '' }}">
                            <label class="required" for="paciente_id">{{ trans('cruds.citum.fields.paciente') }}</label>
                            <select class="form-control select2" name="paciente_id" id="paciente_id" required>
                                @foreach($pacientes as $id => $entry)
                                    <option value="{{ $id }}" {{ old('paciente_id') == $id ? 'selected' : '' }}>{{ $entry }}</option>
                                @endforeach
                            </select>
                            @if($errors->has('paciente'))
                                <span class="help-block" role="alert">{{ $errors->first('paciente') }}</span>
                            @endif
                            <span class="help-block">{{ trans('cruds.citum.fields.paciente_helper') }}</span>
                        </div>
                        <div class="form-group {{ $errors->has('medico') ? 'has-error' : '' }}">
                            <label class="required" for="medico_id">{{ trans('cruds.citum.fields.medico') }}</label>
                            <select class="form-control select2" name="medico_id" id="medico_id" required>
                                @foreach($medicos as $id => $entry)
                                    <option value="{{ $id }}" {{ old('medico_id') == $id ? 'selected' : '' }}>{{ $entry }}</option>
                                @endforeach
                            </select>
                            @if($errors->has('medico'))
                                <span class="help-block" role="alert">{{ $errors->first('medico') }}</span>
                            @endif
                            <span class="help-block">{{ trans('cruds.citum.fields.medico_helper') }}</span>
                        </div>
                        <div class="form-group {{ $errors->has('departamento') ? 'has-error' : '' }}">
                            <label class="required" for="departamento_id">{{ trans('cruds.citum.fields.departamento') }}</label>
                            <select class="form-control select2" name="departamento_id" id="departamento_id" required>
                                @foreach($departamentos as $id => $entry)
                                    <option value="{{ $id }}" {{ old('departamento_id') == $id ? 'selected' : '' }}>{{ $entry }}</option>
                                @endforeach
                            </select>
                            @if($errors->has('departamento'))
                                <span class="help-block" role="alert">{{ $errors->first('departamento') }}</span>
                            @endif
                            <span class="help-block">{{ trans('cruds.citum.fields.departamento_helper') }}</span>
                        </div>
                        <div class="form-group {{ $errors->has('opd_fecha') ? 'has-error' : '' }}">
                            <label class="required" for="opd_fecha">{{ trans('cruds.citum.fields.opd_fecha') }}</label>
                            <input class="form-control date" type="text" name="opd_fecha" id="opd_fecha" value="{{ old('opd_fecha') }}" required>
                            @if($errors->has('opd_fecha'))
                                <span class="help-block" role="alert">{{ $errors->first('opd_fecha') }}</span>
                            @endif
                            <span class="help-block">{{ trans('cruds.citum.fields.opd_fecha_helper') }}</span>
                        </div>
                        <div class="form-group {{ $errors->has('problema') ? 'has-error' : '' }}">
                            <label for="problema">{{ trans('cruds.citum.fields.problema') }}</label>
                            <textarea class="form-control" name="problema" id="problema">{{ old('problema') }}</textarea>
                            @if($errors->has('problema'))
                                <span class="help-block" role="alert">{{ $errors->first('problema') }}</span>
                            @endif
                            <span class="help-block">{{ trans('cruds.citum.fields.problema_helper') }}</span>
                        </div>
                        <div class="form-group">
                            <button class="btn btn-danger" type="submit">
                                {{ trans('global.save') }}
                            </button>
                        </div>
                    </form>
                </div>
            </div>



        </div>
    </div>
</div>
@endsection