@extends('layouts.admin')
@section('content')
<div class="content">
    @can('citum_create')
        <div style="margin-bottom: 10px;" class="row">
            <div class="col-lg-12">
                <a class="btn btn-success" href="{{ route('admin.cita.create') }}">
                    {{ trans('global.add') }} {{ trans('cruds.citum.title_singular') }}
                </a>
            </div>
        </div>
    @endcan
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    {{ trans('cruds.citum.title_singular') }} {{ trans('global.list') }}
                </div>
                <div class="panel-body">
                    <div class="table-responsive">
                        <table class=" table table-bordered table-striped table-hover datatable datatable-Citum">
                            <thead>
                                <tr>
                                    <th width="10">

                                    </th>
                                    <th>
                                        {{ trans('cruds.citum.fields.id') }}
                                    </th>
                                    <th>
                                        {{ trans('cruds.citum.fields.paciente') }}
                                    </th>
                                    <th>
                                        {{ trans('cruds.citum.fields.medico') }}
                                    </th>
                                    <th>
                                        {{ trans('cruds.citum.fields.departamento') }}
                                    </th>
                                    <th>
                                        {{ trans('cruds.citum.fields.opd_fecha') }}
                                    </th>
                                    <th>
                                        {{ trans('cruds.citum.fields.problema') }}
                                    </th>
                                    <th>
                                        &nbsp;
                                    </th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($cita as $key => $citum)
                                    <tr data-entry-id="{{ $citum->id }}">
                                        <td>

                                        </td>
                                        <td>
                                            {{ $citum->id ?? '' }}
                                        </td>
                                        <td>
                                            {{ $citum->paciente->name ?? '' }}
                                        </td>
                                        <td>
                                            {{ $citum->medico->especialista ?? '' }}
                                        </td>
                                        <td>
                                            {{ $citum->departamento->nombre ?? '' }}
                                        </td>
                                        <td>
                                            {{ $citum->opd_fecha ?? '' }}
                                        </td>
                                        <td>
                                            {{ $citum->problema ?? '' }}
                                        </td>
                                        <td>
                                            @can('citum_show')
                                                <a class="btn btn-xs btn-primary" href="{{ route('admin.cita.show', $citum->id) }}">
                                                    {{ trans('global.view') }}
                                                </a>
                                            @endcan

                                            @can('citum_edit')
                                                <a class="btn btn-xs btn-info" href="{{ route('admin.cita.edit', $citum->id) }}">
                                                    {{ trans('global.edit') }}
                                                </a>
                                            @endcan

                                            @can('citum_delete')
                                                <form action="{{ route('admin.cita.destroy', $citum->id) }}" method="POST" onsubmit="return confirm('{{ trans('global.areYouSure') }}');" style="display: inline-block;">
                                                    <input type="hidden" name="_method" value="DELETE">
                                                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                                    <input type="submit" class="btn btn-xs btn-danger" value="{{ trans('global.delete') }}">
                                                </form>
                                            @endcan

                                        </td>

                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>



        </div>
    </div>
</div>
@endsection
@section('scripts')
@parent
<script>
    $(function () {
  let dtButtons = $.extend(true, [], $.fn.dataTable.defaults.buttons)
@can('citum_delete')
  let deleteButtonTrans = '{{ trans('global.datatables.delete') }}'
  let deleteButton = {
    text: deleteButtonTrans,
    url: "{{ route('admin.cita.massDestroy') }}",
    className: 'btn-danger',
    action: function (e, dt, node, config) {
      var ids = $.map(dt.rows({ selected: true }).nodes(), function (entry) {
          return $(entry).data('entry-id')
      });

      if (ids.length === 0) {
        alert('{{ trans('global.datatables.zero_selected') }}')

        return
      }

      if (confirm('{{ trans('global.areYouSure') }}')) {
        $.ajax({
          headers: {'x-csrf-token': _token},
          method: 'POST',
          url: config.url,
          data: { ids: ids, _method: 'DELETE' }})
          .done(function () { location.reload() })
      }
    }
  }
  dtButtons.push(deleteButton)
@endcan

  $.extend(true, $.fn.dataTable.defaults, {
    orderCellsTop: true,
    order: [[ 1, 'desc' ]],
    pageLength: 100,
  });
  let table = $('.datatable-Citum:not(.ajaxTable)').DataTable({ buttons: dtButtons })
  $('a[data-toggle="tab"]').on('shown.bs.tab click', function(e){
      $($.fn.dataTable.tables(true)).DataTable()
          .columns.adjust();
  });
  
})

</script>
@endsection