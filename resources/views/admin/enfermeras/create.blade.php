@extends('layouts.admin')
@section('content')
<div class="content">

    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    {{ trans('global.create') }} {{ trans('cruds.enfermera.title_singular') }}
                </div>
                <div class="panel-body">
                    <form method="POST" action="{{ route("admin.enfermeras.store") }}" enctype="multipart/form-data">
                        @csrf
                        <div class="form-group {{ $errors->has('usuario') ? 'has-error' : '' }}">
                            <label class="required" for="usuario_id">{{ trans('cruds.enfermera.fields.usuario') }}</label>
                            <select class="form-control select2" name="usuario_id" id="usuario_id" required>
                                @foreach($usuarios as $id => $entry)
                                    <option value="{{ $id }}" {{ old('usuario_id') == $id ? 'selected' : '' }}>{{ $entry }}</option>
                                @endforeach
                            </select>
                            @if($errors->has('usuario'))
                                <span class="help-block" role="alert">{{ $errors->first('usuario') }}</span>
                            @endif
                            <span class="help-block">{{ trans('cruds.enfermera.fields.usuario_helper') }}</span>
                        </div>
                        <div class="form-group">
                            <button class="btn btn-danger" type="submit">
                                {{ trans('global.save') }}
                            </button>
                        </div>
                    </form>
                </div>
            </div>



        </div>
    </div>
</div>
@endsection