@extends('layouts.admin')
@section('content')
<div class="content">

    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    {{ trans('global.show') }} {{ trans('cruds.casosDePaciente.title') }}
                </div>
                <div class="panel-body">
                    <div class="form-group">
                        <div class="form-group">
                            <a class="btn btn-default" href="{{ route('admin.casos-de-pacientes.index') }}">
                                {{ trans('global.back_to_list') }}
                            </a>
                        </div>
                        <table class="table table-bordered table-striped">
                            <tbody>
                                <tr>
                                    <th>
                                        {{ trans('cruds.casosDePaciente.fields.id') }}
                                    </th>
                                    <td>
                                        {{ $casosDePaciente->id }}
                                    </td>
                                </tr>
                                <tr>
                                    <th>
                                        {{ trans('cruds.casosDePaciente.fields.telefono') }}
                                    </th>
                                    <td>
                                        {{ $casosDePaciente->telefono }}
                                    </td>
                                </tr>
                                <tr>
                                    <th>
                                        {{ trans('cruds.casosDePaciente.fields.tarifa') }}
                                    </th>
                                    <td>
                                        {{ $casosDePaciente->tarifa }}
                                    </td>
                                </tr>
                                <tr>
                                    <th>
                                        {{ trans('cruds.casosDePaciente.fields.descripcion') }}
                                    </th>
                                    <td>
                                        {{ $casosDePaciente->descripcion }}
                                    </td>
                                </tr>
                                <tr>
                                    <th>
                                        {{ trans('cruds.casosDePaciente.fields.medico') }}
                                    </th>
                                    <td>
                                        {{ $casosDePaciente->medico->especialista ?? '' }}
                                    </td>
                                </tr>
                                <tr>
                                    <th>
                                        {{ trans('cruds.casosDePaciente.fields.paciente') }}
                                    </th>
                                    <td>
                                        {{ $casosDePaciente->paciente->name ?? '' }}
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                        <div class="form-group">
                            <a class="btn btn-default" href="{{ route('admin.casos-de-pacientes.index') }}">
                                {{ trans('global.back_to_list') }}
                            </a>
                        </div>
                    </div>
                </div>
            </div>



        </div>
    </div>
</div>
@endsection