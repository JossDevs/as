@extends('layouts.admin')
@section('content')
<div class="content">
    @can('casos_de_paciente_create')
        <div style="margin-bottom: 10px;" class="row">
            <div class="col-lg-12">
                <a class="btn btn-success" href="{{ route('admin.casos-de-pacientes.create') }}">
                    {{ trans('global.add') }} {{ trans('cruds.casosDePaciente.title_singular') }}
                </a>
            </div>
        </div>
    @endcan
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    {{ trans('cruds.casosDePaciente.title_singular') }} {{ trans('global.list') }}
                </div>
                <div class="panel-body">
                    <div class="table-responsive">
                        <table class=" table table-bordered table-striped table-hover datatable datatable-CasosDePaciente">
                            <thead>
                                <tr>
                                    <th width="10">

                                    </th>
                                    <th>
                                        {{ trans('cruds.casosDePaciente.fields.id') }}
                                    </th>
                                    <th>
                                        {{ trans('cruds.casosDePaciente.fields.telefono') }}
                                    </th>
                                    <th>
                                        {{ trans('cruds.casosDePaciente.fields.tarifa') }}
                                    </th>
                                    <th>
                                        {{ trans('cruds.casosDePaciente.fields.descripcion') }}
                                    </th>
                                    <th>
                                        {{ trans('cruds.casosDePaciente.fields.medico') }}
                                    </th>
                                    <th>
                                        {{ trans('cruds.casosDePaciente.fields.paciente') }}
                                    </th>
                                    <th>
                                        &nbsp;
                                    </th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($casosDePacientes as $key => $casosDePaciente)
                                    <tr data-entry-id="{{ $casosDePaciente->id }}">
                                        <td>

                                        </td>
                                        <td>
                                            {{ $casosDePaciente->id ?? '' }}
                                        </td>
                                        <td>
                                            {{ $casosDePaciente->telefono ?? '' }}
                                        </td>
                                        <td>
                                            {{ $casosDePaciente->tarifa ?? '' }}
                                        </td>
                                        <td>
                                            {{ $casosDePaciente->descripcion ?? '' }}
                                        </td>
                                        <td>
                                            {{ $casosDePaciente->medico->especialista ?? '' }}
                                        </td>
                                        <td>
                                            {{ $casosDePaciente->paciente->name ?? '' }}
                                        </td>
                                        <td>
                                            @can('casos_de_paciente_show')
                                                <a class="btn btn-xs btn-primary" href="{{ route('admin.casos-de-pacientes.show', $casosDePaciente->id) }}">
                                                    {{ trans('global.view') }}
                                                </a>
                                            @endcan

                                            @can('casos_de_paciente_edit')
                                                <a class="btn btn-xs btn-info" href="{{ route('admin.casos-de-pacientes.edit', $casosDePaciente->id) }}">
                                                    {{ trans('global.edit') }}
                                                </a>
                                            @endcan

                                            @can('casos_de_paciente_delete')
                                                <form action="{{ route('admin.casos-de-pacientes.destroy', $casosDePaciente->id) }}" method="POST" onsubmit="return confirm('{{ trans('global.areYouSure') }}');" style="display: inline-block;">
                                                    <input type="hidden" name="_method" value="DELETE">
                                                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                                    <input type="submit" class="btn btn-xs btn-danger" value="{{ trans('global.delete') }}">
                                                </form>
                                            @endcan

                                        </td>

                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>



        </div>
    </div>
</div>
@endsection
@section('scripts')
@parent
<script>
    $(function () {
  let dtButtons = $.extend(true, [], $.fn.dataTable.defaults.buttons)
@can('casos_de_paciente_delete')
  let deleteButtonTrans = '{{ trans('global.datatables.delete') }}'
  let deleteButton = {
    text: deleteButtonTrans,
    url: "{{ route('admin.casos-de-pacientes.massDestroy') }}",
    className: 'btn-danger',
    action: function (e, dt, node, config) {
      var ids = $.map(dt.rows({ selected: true }).nodes(), function (entry) {
          return $(entry).data('entry-id')
      });

      if (ids.length === 0) {
        alert('{{ trans('global.datatables.zero_selected') }}')

        return
      }

      if (confirm('{{ trans('global.areYouSure') }}')) {
        $.ajax({
          headers: {'x-csrf-token': _token},
          method: 'POST',
          url: config.url,
          data: { ids: ids, _method: 'DELETE' }})
          .done(function () { location.reload() })
      }
    }
  }
  dtButtons.push(deleteButton)
@endcan

  $.extend(true, $.fn.dataTable.defaults, {
    orderCellsTop: true,
    order: [[ 1, 'desc' ]],
    pageLength: 100,
  });
  let table = $('.datatable-CasosDePaciente:not(.ajaxTable)').DataTable({ buttons: dtButtons })
  $('a[data-toggle="tab"]').on('shown.bs.tab click', function(e){
      $($.fn.dataTable.tables(true)).DataTable()
          .columns.adjust();
  });
  
})

</script>
@endsection