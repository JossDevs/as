@extends('layouts.admin')
@section('content')
<div class="content">

    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    {{ trans('global.edit') }} {{ trans('cruds.casosDePaciente.title_singular') }}
                </div>
                <div class="panel-body">
                    <form method="POST" action="{{ route("admin.casos-de-pacientes.update", [$casosDePaciente->id]) }}" enctype="multipart/form-data">
                        @method('PUT')
                        @csrf
                        <div class="form-group {{ $errors->has('telefono') ? 'has-error' : '' }}">
                            <label class="required" for="telefono">{{ trans('cruds.casosDePaciente.fields.telefono') }}</label>
                            <input class="form-control" type="text" name="telefono" id="telefono" value="{{ old('telefono', $casosDePaciente->telefono) }}" required>
                            @if($errors->has('telefono'))
                                <span class="help-block" role="alert">{{ $errors->first('telefono') }}</span>
                            @endif
                            <span class="help-block">{{ trans('cruds.casosDePaciente.fields.telefono_helper') }}</span>
                        </div>
                        <div class="form-group {{ $errors->has('tarifa') ? 'has-error' : '' }}">
                            <label class="required" for="tarifa">{{ trans('cruds.casosDePaciente.fields.tarifa') }}</label>
                            <input class="form-control" type="text" name="tarifa" id="tarifa" value="{{ old('tarifa', $casosDePaciente->tarifa) }}" required>
                            @if($errors->has('tarifa'))
                                <span class="help-block" role="alert">{{ $errors->first('tarifa') }}</span>
                            @endif
                            <span class="help-block">{{ trans('cruds.casosDePaciente.fields.tarifa_helper') }}</span>
                        </div>
                        <div class="form-group {{ $errors->has('descripcion') ? 'has-error' : '' }}">
                            <label class="required" for="descripcion">{{ trans('cruds.casosDePaciente.fields.descripcion') }}</label>
                            <input class="form-control" type="text" name="descripcion" id="descripcion" value="{{ old('descripcion', $casosDePaciente->descripcion) }}" required>
                            @if($errors->has('descripcion'))
                                <span class="help-block" role="alert">{{ $errors->first('descripcion') }}</span>
                            @endif
                            <span class="help-block">{{ trans('cruds.casosDePaciente.fields.descripcion_helper') }}</span>
                        </div>
                        <div class="form-group {{ $errors->has('medico') ? 'has-error' : '' }}">
                            <label class="required" for="medico_id">{{ trans('cruds.casosDePaciente.fields.medico') }}</label>
                            <select class="form-control select2" name="medico_id" id="medico_id" required>
                                @foreach($medicos as $id => $entry)
                                    <option value="{{ $id }}" {{ (old('medico_id') ? old('medico_id') : $casosDePaciente->medico->id ?? '') == $id ? 'selected' : '' }}>{{ $entry }}</option>
                                @endforeach
                            </select>
                            @if($errors->has('medico'))
                                <span class="help-block" role="alert">{{ $errors->first('medico') }}</span>
                            @endif
                            <span class="help-block">{{ trans('cruds.casosDePaciente.fields.medico_helper') }}</span>
                        </div>
                        <div class="form-group {{ $errors->has('paciente') ? 'has-error' : '' }}">
                            <label class="required" for="paciente_id">{{ trans('cruds.casosDePaciente.fields.paciente') }}</label>
                            <select class="form-control select2" name="paciente_id" id="paciente_id" required>
                                @foreach($pacientes as $id => $entry)
                                    <option value="{{ $id }}" {{ (old('paciente_id') ? old('paciente_id') : $casosDePaciente->paciente->id ?? '') == $id ? 'selected' : '' }}>{{ $entry }}</option>
                                @endforeach
                            </select>
                            @if($errors->has('paciente'))
                                <span class="help-block" role="alert">{{ $errors->first('paciente') }}</span>
                            @endif
                            <span class="help-block">{{ trans('cruds.casosDePaciente.fields.paciente_helper') }}</span>
                        </div>
                        <div class="form-group">
                            <button class="btn btn-danger" type="submit">
                                {{ trans('global.save') }}
                            </button>
                        </div>
                    </form>
                </div>
            </div>



        </div>
    </div>
</div>
@endsection