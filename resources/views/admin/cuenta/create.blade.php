@extends('layouts.admin')
@section('content')
<div class="content">

    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    {{ trans('global.create') }} {{ trans('cruds.cuentum.title_singular') }}
                </div>
                <div class="panel-body">
                    <form method="POST" action="{{ route("admin.cuenta.store") }}" enctype="multipart/form-data">
                        @csrf
                        <div class="form-group {{ $errors->has('paciente') ? 'has-error' : '' }}">
                            <label class="required" for="paciente_id">{{ trans('cruds.cuentum.fields.paciente') }}</label>
                            <select class="form-control select2" name="paciente_id" id="paciente_id" required>
                                @foreach($pacientes as $id => $entry)
                                    <option value="{{ $id }}" {{ old('paciente_id') == $id ? 'selected' : '' }}>{{ $entry }}</option>
                                @endforeach
                            </select>
                            @if($errors->has('paciente'))
                                <span class="help-block" role="alert">{{ $errors->first('paciente') }}</span>
                            @endif
                            <span class="help-block">{{ trans('cruds.cuentum.fields.paciente_helper') }}</span>
                        </div>
                        <div class="form-group {{ $errors->has('fecha_factura') ? 'has-error' : '' }}">
                            <label class="required" for="fecha_factura">{{ trans('cruds.cuentum.fields.fecha_factura') }}</label>
                            <input class="form-control date" type="text" name="fecha_factura" id="fecha_factura" value="{{ old('fecha_factura') }}" required>
                            @if($errors->has('fecha_factura'))
                                <span class="help-block" role="alert">{{ $errors->first('fecha_factura') }}</span>
                            @endif
                            <span class="help-block">{{ trans('cruds.cuentum.fields.fecha_factura_helper') }}</span>
                        </div>
                        <div class="form-group {{ $errors->has('importe') ? 'has-error' : '' }}">
                            <label class="required" for="importe">{{ trans('cruds.cuentum.fields.importe') }}</label>
                            <input class="form-control" type="number" name="importe" id="importe" value="{{ old('importe', '') }}" step="0.01" required>
                            @if($errors->has('importe'))
                                <span class="help-block" role="alert">{{ $errors->first('importe') }}</span>
                            @endif
                            <span class="help-block">{{ trans('cruds.cuentum.fields.importe_helper') }}</span>
                        </div>
                        <div class="form-group">
                            <button class="btn btn-danger" type="submit">
                                {{ trans('global.save') }}
                            </button>
                        </div>
                    </form>
                </div>
            </div>



        </div>
    </div>
</div>
@endsection