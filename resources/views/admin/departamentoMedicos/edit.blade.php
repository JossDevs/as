@extends('layouts.admin')
@section('content')
<div class="content">

    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    {{ trans('global.edit') }} {{ trans('cruds.departamentoMedico.title_singular') }}
                </div>
                <div class="panel-body">
                    <form method="POST" action="{{ route("admin.departamento-medicos.update", [$departamentoMedico->id]) }}" enctype="multipart/form-data">
                        @method('PUT')
                        @csrf
                        <div class="form-group {{ $errors->has('titulo') ? 'has-error' : '' }}">
                            <label class="required" for="titulo">{{ trans('cruds.departamentoMedico.fields.titulo') }}</label>
                            <input class="form-control" type="text" name="titulo" id="titulo" value="{{ old('titulo', $departamentoMedico->titulo) }}" required>
                            @if($errors->has('titulo'))
                                <span class="help-block" role="alert">{{ $errors->first('titulo') }}</span>
                            @endif
                            <span class="help-block">{{ trans('cruds.departamentoMedico.fields.titulo_helper') }}</span>
                        </div>
                        <div class="form-group {{ $errors->has('descripcion') ? 'has-error' : '' }}">
                            <label for="descripcion">{{ trans('cruds.departamentoMedico.fields.descripcion') }}</label>
                            <textarea class="form-control" name="descripcion" id="descripcion">{{ old('descripcion', $departamentoMedico->descripcion) }}</textarea>
                            @if($errors->has('descripcion'))
                                <span class="help-block" role="alert">{{ $errors->first('descripcion') }}</span>
                            @endif
                            <span class="help-block">{{ trans('cruds.departamentoMedico.fields.descripcion_helper') }}</span>
                        </div>
                        <div class="form-group">
                            <button class="btn btn-danger" type="submit">
                                {{ trans('global.save') }}
                            </button>
                        </div>
                    </form>
                </div>
            </div>



        </div>
    </div>
</div>
@endsection