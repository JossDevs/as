@extends('layouts.admin')
@section('content')
<div class="content">

    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    {{ trans('global.show') }} {{ trans('cruds.departamentoMedico.title') }}
                </div>
                <div class="panel-body">
                    <div class="form-group">
                        <div class="form-group">
                            <a class="btn btn-default" href="{{ route('admin.departamento-medicos.index') }}">
                                {{ trans('global.back_to_list') }}
                            </a>
                        </div>
                        <table class="table table-bordered table-striped">
                            <tbody>
                                <tr>
                                    <th>
                                        {{ trans('cruds.departamentoMedico.fields.id') }}
                                    </th>
                                    <td>
                                        {{ $departamentoMedico->id }}
                                    </td>
                                </tr>
                                <tr>
                                    <th>
                                        {{ trans('cruds.departamentoMedico.fields.titulo') }}
                                    </th>
                                    <td>
                                        {{ $departamentoMedico->titulo }}
                                    </td>
                                </tr>
                                <tr>
                                    <th>
                                        {{ trans('cruds.departamentoMedico.fields.descripcion') }}
                                    </th>
                                    <td>
                                        {{ $departamentoMedico->descripcion }}
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                        <div class="form-group">
                            <a class="btn btn-default" href="{{ route('admin.departamento-medicos.index') }}">
                                {{ trans('global.back_to_list') }}
                            </a>
                        </div>
                    </div>
                </div>
            </div>

            <div class="panel panel-default">
                <div class="panel-heading">
                    {{ trans('global.relatedData') }}
                </div>
                <ul class="nav nav-tabs" role="tablist" id="relationship-tabs">
                    <li role="presentation">
                        <a href="#departamento_medico_medicos" aria-controls="departamento_medico_medicos" role="tab" data-toggle="tab">
                            {{ trans('cruds.medico.title') }}
                        </a>
                    </li>
                </ul>
                <div class="tab-content">
                    <div class="tab-pane" role="tabpanel" id="departamento_medico_medicos">
                        @includeIf('admin.departamentoMedicos.relationships.departamentoMedicoMedicos', ['medicos' => $departamentoMedico->departamentoMedicoMedicos])
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>
@endsection