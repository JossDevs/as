@extends('layouts.admin')
@section('content')
<div class="content">

    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    {{ trans('global.show') }} {{ trans('cruds.medico.title') }}
                </div>
                <div class="panel-body">
                    <div class="form-group">
                        <div class="form-group">
                            <a class="btn btn-default" href="{{ route('admin.medicos.index') }}">
                                {{ trans('global.back_to_list') }}
                            </a>
                        </div>
                        <table class="table table-bordered table-striped">
                            <tbody>
                                <tr>
                                    <th>
                                        {{ trans('cruds.medico.fields.id') }}
                                    </th>
                                    <td>
                                        {{ $medico->id }}
                                    </td>
                                </tr>
                                <tr>
                                    <th>
                                        {{ trans('cruds.medico.fields.usuario') }}
                                    </th>
                                    <td>
                                        {{ $medico->usuario->name ?? '' }}
                                    </td>
                                </tr>
                                <tr>
                                    <th>
                                        {{ trans('cruds.medico.fields.departamento_medico') }}
                                    </th>
                                    <td>
                                        {{ $medico->departamento_medico->titulo ?? '' }}
                                    </td>
                                </tr>
                                <tr>
                                    <th>
                                        {{ trans('cruds.medico.fields.especialista') }}
                                    </th>
                                    <td>
                                        {{ $medico->especialista }}
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                        <div class="form-group">
                            <a class="btn btn-default" href="{{ route('admin.medicos.index') }}">
                                {{ trans('global.back_to_list') }}
                            </a>
                        </div>
                    </div>
                </div>
            </div>

            <div class="panel panel-default">
                <div class="panel-heading">
                    {{ trans('global.relatedData') }}
                </div>
                <ul class="nav nav-tabs" role="tablist" id="relationship-tabs">
                    <li role="presentation">
                        <a href="#medico_cita" aria-controls="medico_cita" role="tab" data-toggle="tab">
                            {{ trans('cruds.citum.title') }}
                        </a>
                    </li>
                </ul>
                <div class="tab-content">
                    <div class="tab-pane" role="tabpanel" id="medico_cita">
                        @includeIf('admin.medicos.relationships.medicoCita', ['cita' => $medico->medicoCita])
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>
@endsection