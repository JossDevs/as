@extends('layouts.admin')
@section('content')
<div class="content">

    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    {{ trans('global.create') }} {{ trans('cruds.medico.title_singular') }}
                </div>
                <div class="panel-body">
                    <form method="POST" action="{{ route("admin.medicos.store") }}" enctype="multipart/form-data">
                        @csrf
                        <div class="form-group {{ $errors->has('usuario') ? 'has-error' : '' }}">
                            <label class="required" for="usuario_id">{{ trans('cruds.medico.fields.usuario') }}</label>
                            <select class="form-control select2" name="usuario_id" id="usuario_id" required>
                                @foreach($usuarios as $id => $entry)
                                    <option value="{{ $id }}" {{ old('usuario_id') == $id ? 'selected' : '' }}>{{ $entry }}</option>
                                @endforeach
                            </select>
                            @if($errors->has('usuario'))
                                <span class="help-block" role="alert">{{ $errors->first('usuario') }}</span>
                            @endif
                            <span class="help-block">{{ trans('cruds.medico.fields.usuario_helper') }}</span>
                        </div>
                        <div class="form-group {{ $errors->has('departamento_medico') ? 'has-error' : '' }}">
                            <label class="required" for="departamento_medico_id">{{ trans('cruds.medico.fields.departamento_medico') }}</label>
                            <select class="form-control select2" name="departamento_medico_id" id="departamento_medico_id" required>
                                @foreach($departamento_medicos as $id => $entry)
                                    <option value="{{ $id }}" {{ old('departamento_medico_id') == $id ? 'selected' : '' }}>{{ $entry }}</option>
                                @endforeach
                            </select>
                            @if($errors->has('departamento_medico'))
                                <span class="help-block" role="alert">{{ $errors->first('departamento_medico') }}</span>
                            @endif
                            <span class="help-block">{{ trans('cruds.medico.fields.departamento_medico_helper') }}</span>
                        </div>
                        <div class="form-group {{ $errors->has('especialista') ? 'has-error' : '' }}">
                            <label class="required" for="especialista">{{ trans('cruds.medico.fields.especialista') }}</label>
                            <input class="form-control" type="text" name="especialista" id="especialista" value="{{ old('especialista', '') }}" required>
                            @if($errors->has('especialista'))
                                <span class="help-block" role="alert">{{ $errors->first('especialista') }}</span>
                            @endif
                            <span class="help-block">{{ trans('cruds.medico.fields.especialista_helper') }}</span>
                        </div>
                        <div class="form-group">
                            <button class="btn btn-danger" type="submit">
                                {{ trans('global.save') }}
                            </button>
                        </div>
                    </form>
                </div>
            </div>



        </div>
    </div>
</div>
@endsection