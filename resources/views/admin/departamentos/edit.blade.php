@extends('layouts.admin')
@section('content')
<div class="content">

    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    {{ trans('global.edit') }} {{ trans('cruds.departamento.title_singular') }}
                </div>
                <div class="panel-body">
                    <form method="POST" action="{{ route("admin.departamentos.update", [$departamento->id]) }}" enctype="multipart/form-data">
                        @method('PUT')
                        @csrf
                        <div class="form-group {{ $errors->has('nombre') ? 'has-error' : '' }}">
                            <label class="required" for="nombre">{{ trans('cruds.departamento.fields.nombre') }}</label>
                            <input class="form-control" type="text" name="nombre" id="nombre" value="{{ old('nombre', $departamento->nombre) }}" required>
                            @if($errors->has('nombre'))
                                <span class="help-block" role="alert">{{ $errors->first('nombre') }}</span>
                            @endif
                            <span class="help-block">{{ trans('cruds.departamento.fields.nombre_helper') }}</span>
                        </div>
                        <div class="form-group {{ $errors->has('nombre_guardia') ? 'has-error' : '' }}">
                            <label class="required" for="nombre_guardia">{{ trans('cruds.departamento.fields.nombre_guardia') }}</label>
                            <input class="form-control" type="text" name="nombre_guardia" id="nombre_guardia" value="{{ old('nombre_guardia', $departamento->nombre_guardia) }}" required>
                            @if($errors->has('nombre_guardia'))
                                <span class="help-block" role="alert">{{ $errors->first('nombre_guardia') }}</span>
                            @endif
                            <span class="help-block">{{ trans('cruds.departamento.fields.nombre_guardia_helper') }}</span>
                        </div>
                        <div class="form-group">
                            <button class="btn btn-danger" type="submit">
                                {{ trans('global.save') }}
                            </button>
                        </div>
                    </form>
                </div>
            </div>



        </div>
    </div>
</div>
@endsection