<aside class="main-sidebar">
    <section class="sidebar" style="height: auto;">
        <ul class="sidebar-menu tree" data-widget="tree">
            <li>
                <a href="{{ route("admin.home") }}">
                    <i class="fas fa-fw fa-tachometer-alt">

                    </i>
                    {{ trans('global.dashboard') }}
                </a>
            </li>
            @can('user_management_access')
                <li class="treeview">
                    <a href="#">
                        <i class="fa-fw fas fa-users">

                        </i>
                        <span>{{ trans('cruds.userManagement.title') }}</span>
                        <span class="pull-right-container"><i class="fa fa-fw fa-angle-left pull-right"></i></span>
                    </a>
                    <ul class="treeview-menu">
                        @can('permission_access')
                            <li class="{{ request()->is("admin/permissions") || request()->is("admin/permissions/*") ? "active" : "" }}">
                                <a href="{{ route("admin.permissions.index") }}">
                                    <i class="fa-fw fas fa-unlock-alt">

                                    </i>
                                    <span>{{ trans('cruds.permission.title') }}</span>

                                </a>
                            </li>
                        @endcan
                        @can('role_access')
                            <li class="{{ request()->is("admin/roles") || request()->is("admin/roles/*") ? "active" : "" }}">
                                <a href="{{ route("admin.roles.index") }}">
                                    <i class="fa-fw fas fa-briefcase">

                                    </i>
                                    <span>{{ trans('cruds.role.title') }}</span>

                                </a>
                            </li>
                        @endcan
                        @can('user_access')
                            <li class="{{ request()->is("admin/users") || request()->is("admin/users/*") ? "active" : "" }}">
                                <a href="{{ route("admin.users.index") }}">
                                    <i class="fa-fw fas fa-user">

                                    </i>
                                    <span>{{ trans('cruds.user.title') }}</span>

                                </a>
                            </li>
                        @endcan
                        @can('audit_log_access')
                            <li class="{{ request()->is("admin/audit-logs") || request()->is("admin/audit-logs/*") ? "active" : "" }}">
                                <a href="{{ route("admin.audit-logs.index") }}">
                                    <i class="fa-fw fas fa-file-alt">

                                    </i>
                                    <span>{{ trans('cruds.auditLog.title') }}</span>

                                </a>
                            </li>
                        @endcan
                    </ul>
                </li>
            @endcan
            @can('departamento_access')
                <li class="{{ request()->is("admin/departamentos") || request()->is("admin/departamentos/*") ? "active" : "" }}">
                    <a href="{{ route("admin.departamentos.index") }}">
                        <i class="fa-fw fas fa-cogs">

                        </i>
                        <span>{{ trans('cruds.departamento.title') }}</span>

                    </a>
                </li>
            @endcan
            @can('enfermera_access')
                <li class="{{ request()->is("admin/enfermeras") || request()->is("admin/enfermeras/*") ? "active" : "" }}">
                    <a href="{{ route("admin.enfermeras.index") }}">
                        <i class="fa-fw fas fa-cogs">

                        </i>
                        <span>{{ trans('cruds.enfermera.title') }}</span>

                    </a>
                </li>
            @endcan
            @can('recepcionistum_access')
                <li class="{{ request()->is("admin/recepcionista") || request()->is("admin/recepcionista/*") ? "active" : "" }}">
                    <a href="{{ route("admin.recepcionista.index") }}">
                        <i class="fa-fw fas fa-cogs">

                        </i>
                        <span>{{ trans('cruds.recepcionistum.title') }}</span>

                    </a>
                </li>
            @endcan
            @can('farmacium_access')
                <li class="{{ request()->is("admin/farmacia") || request()->is("admin/farmacia/*") ? "active" : "" }}">
                    <a href="{{ route("admin.farmacia.index") }}">
                        <i class="fa-fw fas fa-cogs">

                        </i>
                        <span>{{ trans('cruds.farmacium.title') }}</span>

                    </a>
                </li>
            @endcan
            @can('medicina_access')
                <li class="treeview">
                    <a href="#">
                        <i class="fa-fw fas fa-cogs">

                        </i>
                        <span>{{ trans('cruds.medicina.title') }}</span>
                        <span class="pull-right-container"><i class="fa fa-fw fa-angle-left pull-right"></i></span>
                    </a>
                    <ul class="treeview-menu">
                        @can('categorium_access')
                            <li class="{{ request()->is("admin/categoria") || request()->is("admin/categoria/*") ? "active" : "" }}">
                                <a href="{{ route("admin.categoria.index") }}">
                                    <i class="fa-fw fas fa-cogs">

                                    </i>
                                    <span>{{ trans('cruds.categorium.title') }}</span>

                                </a>
                            </li>
                        @endcan
                        @can('marca_access')
                            <li class="{{ request()->is("admin/marcas") || request()->is("admin/marcas/*") ? "active" : "" }}">
                                <a href="{{ route("admin.marcas.index") }}">
                                    <i class="fa-fw fas fa-cogs">

                                    </i>
                                    <span>{{ trans('cruds.marca.title') }}</span>

                                </a>
                            </li>
                        @endcan
                        @can('medicamento_access')
                            <li class="{{ request()->is("admin/medicamentos") || request()->is("admin/medicamentos/*") ? "active" : "" }}">
                                <a href="{{ route("admin.medicamentos.index") }}">
                                    <i class="fa-fw fas fa-cogs">

                                    </i>
                                    <span>{{ trans('cruds.medicamento.title') }}</span>

                                </a>
                            </li>
                        @endcan
                    </ul>
                </li>
            @endcan
            @can('medico_parent_access')
                <li class="treeview">
                    <a href="#">
                        <i class="fa-fw fas fa-cogs">

                        </i>
                        <span>{{ trans('cruds.medicoParent.title') }}</span>
                        <span class="pull-right-container"><i class="fa fa-fw fa-angle-left pull-right"></i></span>
                    </a>
                    <ul class="treeview-menu">
                        @can('departamento_medico_access')
                            <li class="{{ request()->is("admin/departamento-medicos") || request()->is("admin/departamento-medicos/*") ? "active" : "" }}">
                                <a href="{{ route("admin.departamento-medicos.index") }}">
                                    <i class="fa-fw fas fa-cogs">

                                    </i>
                                    <span>{{ trans('cruds.departamentoMedico.title') }}</span>

                                </a>
                            </li>
                        @endcan
                        @can('medico_access')
                            <li class="{{ request()->is("admin/medicos") || request()->is("admin/medicos/*") ? "active" : "" }}">
                                <a href="{{ route("admin.medicos.index") }}">
                                    <i class="fa-fw fas fa-cogs">

                                    </i>
                                    <span>{{ trans('cruds.medico.title') }}</span>

                                </a>
                            </li>
                        @endcan
                    </ul>
                </li>
            @endcan
            @can('pacientes_parent_access')
                <li class="treeview">
                    <a href="#">
                        <i class="fa-fw fas fa-cogs">

                        </i>
                        <span>{{ trans('cruds.pacientesParent.title') }}</span>
                        <span class="pull-right-container"><i class="fa fa-fw fa-angle-left pull-right"></i></span>
                    </a>
                    <ul class="treeview-menu">
                        @can('paciente_access')
                            <li class="{{ request()->is("admin/pacientes") || request()->is("admin/pacientes/*") ? "active" : "" }}">
                                <a href="{{ route("admin.pacientes.index") }}">
                                    <i class="fa-fw fas fa-cogs">

                                    </i>
                                    <span>{{ trans('cruds.paciente.title') }}</span>

                                </a>
                            </li>
                        @endcan
                        @can('cuentum_access')
                            <li class="{{ request()->is("admin/cuenta") || request()->is("admin/cuenta/*") ? "active" : "" }}">
                                <a href="{{ route("admin.cuenta.index") }}">
                                    <i class="fa-fw fas fa-cogs">

                                    </i>
                                    <span>{{ trans('cruds.cuentum.title') }}</span>

                                </a>
                            </li>
                        @endcan
                        @can('citum_access')
                            <li class="{{ request()->is("admin/cita") || request()->is("admin/cita/*") ? "active" : "" }}">
                                <a href="{{ route("admin.cita.index") }}">
                                    <i class="fa-fw fas fa-cogs">

                                    </i>
                                    <span>{{ trans('cruds.citum.title') }}</span>

                                </a>
                            </li>
                        @endcan
                        @can('casos_de_paciente_access')
                            <li class="{{ request()->is("admin/casos-de-pacientes") || request()->is("admin/casos-de-pacientes/*") ? "active" : "" }}">
                                <a href="{{ route("admin.casos-de-pacientes.index") }}">
                                    <i class="fa-fw fas fa-cogs">

                                    </i>
                                    <span>{{ trans('cruds.casosDePaciente.title') }}</span>

                                </a>
                            </li>
                        @endcan
                    </ul>
                </li>
            @endcan
            @if(file_exists(app_path('Http/Controllers/Auth/ChangePasswordController.php')))
                @can('profile_password_edit')
                    <li class="{{ request()->is('profile/password') || request()->is('profile/password/*') ? 'active' : '' }}">
                        <a href="{{ route('profile.password.edit') }}">
                            <i class="fa-fw fas fa-key">
                            </i>
                            {{ trans('global.change_password') }}
                        </a>
                    </li>
                @endcan
            @endif
            <li>
                <a href="#" onclick="event.preventDefault(); document.getElementById('logoutform').submit();">
                    <i class="fas fa-fw fa-sign-out-alt">

                    </i>
                    {{ trans('global.logout') }}
                </a>
            </li>
        </ul>
    </section>
</aside>