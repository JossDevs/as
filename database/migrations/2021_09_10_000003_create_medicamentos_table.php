<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMedicamentosTable extends Migration
{
    public function up()
    {
        Schema::create('medicamentos', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('nombre');
            $table->decimal('precio_de_venta', 15, 2);
            $table->decimal('precio_de_compra', 15, 2);
            $table->string('composicion_de_sal');
            $table->longText('descripcion')->nullable();
            $table->longText('efectos_colaterales')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }
}
