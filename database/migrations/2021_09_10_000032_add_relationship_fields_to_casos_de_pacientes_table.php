<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddRelationshipFieldsToCasosDePacientesTable extends Migration
{
    public function up()
    {
        Schema::table('casos_de_pacientes', function (Blueprint $table) {
            $table->unsignedBigInteger('medico_id');
            $table->foreign('medico_id', 'medico_fk_4842450')->references('id')->on('medicos');
            $table->unsignedBigInteger('paciente_id');
            $table->foreign('paciente_id', 'paciente_fk_4842451')->references('id')->on('users');
        });
    }
}
