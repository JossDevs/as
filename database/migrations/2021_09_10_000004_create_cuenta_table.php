<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCuentaTable extends Migration
{
    public function up()
    {
        Schema::create('cuenta', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->date('fecha_factura');
            $table->float('importe', 15, 2);
            $table->timestamps();
            $table->softDeletes();
        });
    }
}
