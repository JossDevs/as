<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddRelationshipFieldsToMedicosTable extends Migration
{
    public function up()
    {
        Schema::table('medicos', function (Blueprint $table) {
            $table->unsignedBigInteger('usuario_id');
            $table->foreign('usuario_id', 'usuario_fk_4842382')->references('id')->on('users');
            $table->unsignedBigInteger('departamento_medico_id');
            $table->foreign('departamento_medico_id', 'departamento_medico_fk_4842383')->references('id')->on('departamento_medicos');
            $table->unsignedBigInteger('created_by_id')->nullable();
            $table->foreign('created_by_id', 'created_by_fk_4842388')->references('id')->on('users');
        });
    }
}
