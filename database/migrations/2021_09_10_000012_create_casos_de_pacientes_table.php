<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCasosDePacientesTable extends Migration
{
    public function up()
    {
        Schema::create('casos_de_pacientes', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('telefono');
            $table->string('tarifa');
            $table->string('descripcion');
            $table->timestamps();
            $table->softDeletes();
        });
    }
}
