<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddRelationshipFieldsToRecepcionistaTable extends Migration
{
    public function up()
    {
        Schema::table('recepcionista', function (Blueprint $table) {
            $table->unsignedBigInteger('usuario_id');
            $table->foreign('usuario_id', 'usuario_fk_4842433')->references('id')->on('users');
            $table->unsignedBigInteger('created_id')->nullable();
            $table->foreign('created_id', 'created_fk_4842437')->references('id')->on('users');
        });
    }
}
