<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddRelationshipFieldsToCitaTable extends Migration
{
    public function up()
    {
        Schema::table('cita', function (Blueprint $table) {
            $table->unsignedBigInteger('paciente_id');
            $table->foreign('paciente_id', 'paciente_fk_4842423')->references('id')->on('users');
            $table->unsignedBigInteger('medico_id');
            $table->foreign('medico_id', 'medico_fk_4842424')->references('id')->on('medicos');
            $table->unsignedBigInteger('departamento_id');
            $table->foreign('departamento_id', 'departamento_fk_4842425')->references('id')->on('departamentos');
            $table->unsignedBigInteger('created_by_id')->nullable();
            $table->foreign('created_by_id', 'created_by_fk_4842431')->references('id')->on('users');
        });
    }
}
