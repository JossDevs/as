<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddRelationshipFieldsToMedicamentosTable extends Migration
{
    public function up()
    {
        Schema::table('medicamentos', function (Blueprint $table) {
            $table->unsignedBigInteger('categoria_id');
            $table->foreign('categoria_id', 'categoria_fk_4842404')->references('id')->on('categoria');
            $table->unsignedBigInteger('marca_id');
            $table->foreign('marca_id', 'marca_fk_4842405')->references('id')->on('marcas');
            $table->unsignedBigInteger('created_by_id')->nullable();
            $table->foreign('created_by_id', 'created_by_fk_4842415')->references('id')->on('users');
        });
    }
}
