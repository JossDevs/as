<?php

namespace Database\Seeders;

use App\Models\Permission;
use Illuminate\Database\Seeder;

class PermissionsTableSeeder extends Seeder
{
    public function run()
    {
        $permissions = [
            [
                'id'    => 1,
                'title' => 'user_management_access',
            ],
            [
                'id'    => 2,
                'title' => 'permission_create',
            ],
            [
                'id'    => 3,
                'title' => 'permission_edit',
            ],
            [
                'id'    => 4,
                'title' => 'permission_show',
            ],
            [
                'id'    => 5,
                'title' => 'permission_delete',
            ],
            [
                'id'    => 6,
                'title' => 'permission_access',
            ],
            [
                'id'    => 7,
                'title' => 'role_create',
            ],
            [
                'id'    => 8,
                'title' => 'role_edit',
            ],
            [
                'id'    => 9,
                'title' => 'role_show',
            ],
            [
                'id'    => 10,
                'title' => 'role_delete',
            ],
            [
                'id'    => 11,
                'title' => 'role_access',
            ],
            [
                'id'    => 12,
                'title' => 'user_create',
            ],
            [
                'id'    => 13,
                'title' => 'user_edit',
            ],
            [
                'id'    => 14,
                'title' => 'user_show',
            ],
            [
                'id'    => 15,
                'title' => 'user_delete',
            ],
            [
                'id'    => 16,
                'title' => 'user_access',
            ],
            [
                'id'    => 17,
                'title' => 'audit_log_show',
            ],
            [
                'id'    => 18,
                'title' => 'audit_log_access',
            ],
            [
                'id'    => 19,
                'title' => 'medicina_access',
            ],
            [
                'id'    => 20,
                'title' => 'categorium_create',
            ],
            [
                'id'    => 21,
                'title' => 'categorium_edit',
            ],
            [
                'id'    => 22,
                'title' => 'categorium_show',
            ],
            [
                'id'    => 23,
                'title' => 'categorium_delete',
            ],
            [
                'id'    => 24,
                'title' => 'categorium_access',
            ],
            [
                'id'    => 25,
                'title' => 'medico_parent_access',
            ],
            [
                'id'    => 26,
                'title' => 'departamento_medico_create',
            ],
            [
                'id'    => 27,
                'title' => 'departamento_medico_edit',
            ],
            [
                'id'    => 28,
                'title' => 'departamento_medico_show',
            ],
            [
                'id'    => 29,
                'title' => 'departamento_medico_delete',
            ],
            [
                'id'    => 30,
                'title' => 'departamento_medico_access',
            ],
            [
                'id'    => 31,
                'title' => 'departamento_create',
            ],
            [
                'id'    => 32,
                'title' => 'departamento_edit',
            ],
            [
                'id'    => 33,
                'title' => 'departamento_show',
            ],
            [
                'id'    => 34,
                'title' => 'departamento_delete',
            ],
            [
                'id'    => 35,
                'title' => 'departamento_access',
            ],
            [
                'id'    => 36,
                'title' => 'marca_create',
            ],
            [
                'id'    => 37,
                'title' => 'marca_edit',
            ],
            [
                'id'    => 38,
                'title' => 'marca_show',
            ],
            [
                'id'    => 39,
                'title' => 'marca_delete',
            ],
            [
                'id'    => 40,
                'title' => 'marca_access',
            ],
            [
                'id'    => 41,
                'title' => 'medico_create',
            ],
            [
                'id'    => 42,
                'title' => 'medico_edit',
            ],
            [
                'id'    => 43,
                'title' => 'medico_show',
            ],
            [
                'id'    => 44,
                'title' => 'medico_delete',
            ],
            [
                'id'    => 45,
                'title' => 'medico_access',
            ],
            [
                'id'    => 46,
                'title' => 'pacientes_parent_access',
            ],
            [
                'id'    => 47,
                'title' => 'paciente_create',
            ],
            [
                'id'    => 48,
                'title' => 'paciente_edit',
            ],
            [
                'id'    => 49,
                'title' => 'paciente_show',
            ],
            [
                'id'    => 50,
                'title' => 'paciente_delete',
            ],
            [
                'id'    => 51,
                'title' => 'paciente_access',
            ],
            [
                'id'    => 52,
                'title' => 'cuentum_create',
            ],
            [
                'id'    => 53,
                'title' => 'cuentum_edit',
            ],
            [
                'id'    => 54,
                'title' => 'cuentum_show',
            ],
            [
                'id'    => 55,
                'title' => 'cuentum_delete',
            ],
            [
                'id'    => 56,
                'title' => 'cuentum_access',
            ],
            [
                'id'    => 57,
                'title' => 'medicamento_create',
            ],
            [
                'id'    => 58,
                'title' => 'medicamento_edit',
            ],
            [
                'id'    => 59,
                'title' => 'medicamento_show',
            ],
            [
                'id'    => 60,
                'title' => 'medicamento_delete',
            ],
            [
                'id'    => 61,
                'title' => 'medicamento_access',
            ],
            [
                'id'    => 62,
                'title' => 'enfermera_create',
            ],
            [
                'id'    => 63,
                'title' => 'enfermera_edit',
            ],
            [
                'id'    => 64,
                'title' => 'enfermera_show',
            ],
            [
                'id'    => 65,
                'title' => 'enfermera_delete',
            ],
            [
                'id'    => 66,
                'title' => 'enfermera_access',
            ],
            [
                'id'    => 67,
                'title' => 'citum_create',
            ],
            [
                'id'    => 68,
                'title' => 'citum_edit',
            ],
            [
                'id'    => 69,
                'title' => 'citum_show',
            ],
            [
                'id'    => 70,
                'title' => 'citum_delete',
            ],
            [
                'id'    => 71,
                'title' => 'citum_access',
            ],
            [
                'id'    => 72,
                'title' => 'recepcionistum_create',
            ],
            [
                'id'    => 73,
                'title' => 'recepcionistum_edit',
            ],
            [
                'id'    => 74,
                'title' => 'recepcionistum_show',
            ],
            [
                'id'    => 75,
                'title' => 'recepcionistum_delete',
            ],
            [
                'id'    => 76,
                'title' => 'recepcionistum_access',
            ],
            [
                'id'    => 77,
                'title' => 'farmacium_create',
            ],
            [
                'id'    => 78,
                'title' => 'farmacium_edit',
            ],
            [
                'id'    => 79,
                'title' => 'farmacium_show',
            ],
            [
                'id'    => 80,
                'title' => 'farmacium_delete',
            ],
            [
                'id'    => 81,
                'title' => 'farmacium_access',
            ],
            [
                'id'    => 82,
                'title' => 'casos_de_paciente_create',
            ],
            [
                'id'    => 83,
                'title' => 'casos_de_paciente_edit',
            ],
            [
                'id'    => 84,
                'title' => 'casos_de_paciente_show',
            ],
            [
                'id'    => 85,
                'title' => 'casos_de_paciente_delete',
            ],
            [
                'id'    => 86,
                'title' => 'casos_de_paciente_access',
            ],
            [
                'id'    => 87,
                'title' => 'profile_password_edit',
            ],
        ];

        Permission::insert($permissions);
    }
}
