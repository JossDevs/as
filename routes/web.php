<?php

Route::redirect('/', '/login');
Route::get('/home', function () {
    if (session('status')) {
        return redirect()->route('admin.home')->with('status', session('status'));
    }

    return redirect()->route('admin.home');
});

Auth::routes(['register' => false]);

Route::group(['prefix' => 'admin', 'as' => 'admin.', 'namespace' => 'Admin', 'middleware' => ['auth']], function () {
    Route::get('/', 'HomeController@index')->name('home');
    // Permissions
    Route::delete('permissions/destroy', 'PermissionsController@massDestroy')->name('permissions.massDestroy');
    Route::resource('permissions', 'PermissionsController');

    // Roles
    Route::delete('roles/destroy', 'RolesController@massDestroy')->name('roles.massDestroy');
    Route::resource('roles', 'RolesController');

    // Users
    Route::delete('users/destroy', 'UsersController@massDestroy')->name('users.massDestroy');
    Route::resource('users', 'UsersController');

    // Audit Logs
    Route::resource('audit-logs', 'AuditLogsController', ['except' => ['create', 'store', 'edit', 'update', 'destroy']]);

    // Categoria
    Route::delete('categoria/destroy', 'CategoriaController@massDestroy')->name('categoria.massDestroy');
    Route::resource('categoria', 'CategoriaController');

    // Departamento Medico
    Route::delete('departamento-medicos/destroy', 'DepartamentoMedicoController@massDestroy')->name('departamento-medicos.massDestroy');
    Route::resource('departamento-medicos', 'DepartamentoMedicoController');

    // Departamento
    Route::delete('departamentos/destroy', 'DepartamentoController@massDestroy')->name('departamentos.massDestroy');
    Route::resource('departamentos', 'DepartamentoController');

    // Marca
    Route::delete('marcas/destroy', 'MarcaController@massDestroy')->name('marcas.massDestroy');
    Route::resource('marcas', 'MarcaController');

    // Medico
    Route::delete('medicos/destroy', 'MedicoController@massDestroy')->name('medicos.massDestroy');
    Route::resource('medicos', 'MedicoController');

    // Paciente
    Route::delete('pacientes/destroy', 'PacienteController@massDestroy')->name('pacientes.massDestroy');
    Route::resource('pacientes', 'PacienteController');

    // Cuenta
    Route::delete('cuenta/destroy', 'CuentaController@massDestroy')->name('cuenta.massDestroy');
    Route::resource('cuenta', 'CuentaController');

    // Medicamento
    Route::delete('medicamentos/destroy', 'MedicamentoController@massDestroy')->name('medicamentos.massDestroy');
    Route::resource('medicamentos', 'MedicamentoController');

    // Enfermera
    Route::delete('enfermeras/destroy', 'EnfermeraController@massDestroy')->name('enfermeras.massDestroy');
    Route::resource('enfermeras', 'EnfermeraController');

    // Cita
    Route::delete('cita/destroy', 'CitaController@massDestroy')->name('cita.massDestroy');
    Route::resource('cita', 'CitaController');

    // Recepcionista
    Route::delete('recepcionista/destroy', 'RecepcionistaController@massDestroy')->name('recepcionista.massDestroy');
    Route::resource('recepcionista', 'RecepcionistaController');

    // Farmacia
    Route::delete('farmacia/destroy', 'FarmaciaController@massDestroy')->name('farmacia.massDestroy');
    Route::resource('farmacia', 'FarmaciaController');

    // Casos De Paciente
    Route::delete('casos-de-pacientes/destroy', 'CasosDePacienteController@massDestroy')->name('casos-de-pacientes.massDestroy');
    Route::resource('casos-de-pacientes', 'CasosDePacienteController');
});
Route::group(['prefix' => 'profile', 'as' => 'profile.', 'namespace' => 'Auth', 'middleware' => ['auth']], function () {
    // Change password
    if (file_exists(app_path('Http/Controllers/Auth/ChangePasswordController.php'))) {
        Route::get('password', 'ChangePasswordController@edit')->name('password.edit');
        Route::post('password', 'ChangePasswordController@update')->name('password.update');
        Route::post('profile', 'ChangePasswordController@updateProfile')->name('password.updateProfile');
        Route::post('profile/destroy', 'ChangePasswordController@destroy')->name('password.destroyProfile');
    }
});
