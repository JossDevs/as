<?php

namespace App\Http\Requests;

use App\Models\CasosDePaciente;
use Gate;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Response;

class StoreCasosDePacienteRequest extends FormRequest
{
    public function authorize()
    {
        return Gate::allows('casos_de_paciente_create');
    }

    public function rules()
    {
        return [
            'telefono' => [
                'string',
                'required',
            ],
            'tarifa' => [
                'string',
                'required',
            ],
            'descripcion' => [
                'string',
                'required',
            ],
            'medico_id' => [
                'required',
                'integer',
            ],
            'paciente_id' => [
                'required',
                'integer',
            ],
        ];
    }
}
