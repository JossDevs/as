<?php

namespace App\Http\Requests;

use App\Models\Citum;
use Gate;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Response;

class UpdateCitumRequest extends FormRequest
{
    public function authorize()
    {
        return Gate::allows('citum_edit');
    }

    public function rules()
    {
        return [
            'paciente_id' => [
                'required',
                'integer',
            ],
            'medico_id' => [
                'required',
                'integer',
            ],
            'departamento_id' => [
                'required',
                'integer',
            ],
            'opd_fecha' => [
                'required',
                'date_format:' . config('panel.date_format'),
            ],
        ];
    }
}
