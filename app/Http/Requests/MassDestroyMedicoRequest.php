<?php

namespace App\Http\Requests;

use App\Models\Medico;
use Gate;
use Illuminate\Foundation\Http\FormRequest;
use Symfony\Component\HttpFoundation\Response;

class MassDestroyMedicoRequest extends FormRequest
{
    public function authorize()
    {
        abort_if(Gate::denies('medico_delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return true;
    }

    public function rules()
    {
        return [
            'ids'   => 'required|array',
            'ids.*' => 'exists:medicos,id',
        ];
    }
}
