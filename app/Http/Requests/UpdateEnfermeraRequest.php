<?php

namespace App\Http\Requests;

use App\Models\Enfermera;
use Gate;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Response;

class UpdateEnfermeraRequest extends FormRequest
{
    public function authorize()
    {
        return Gate::allows('enfermera_edit');
    }

    public function rules()
    {
        return [
            'usuario_id' => [
                'required',
                'integer',
            ],
        ];
    }
}
