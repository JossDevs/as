<?php

namespace App\Http\Requests;

use App\Models\Enfermera;
use Gate;
use Illuminate\Foundation\Http\FormRequest;
use Symfony\Component\HttpFoundation\Response;

class MassDestroyEnfermeraRequest extends FormRequest
{
    public function authorize()
    {
        abort_if(Gate::denies('enfermera_delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return true;
    }

    public function rules()
    {
        return [
            'ids'   => 'required|array',
            'ids.*' => 'exists:enfermeras,id',
        ];
    }
}
