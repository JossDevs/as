<?php

namespace App\Http\Requests;

use App\Models\Medicamento;
use Gate;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Response;

class UpdateMedicamentoRequest extends FormRequest
{
    public function authorize()
    {
        return Gate::allows('medicamento_edit');
    }

    public function rules()
    {
        return [
            'categoria_id' => [
                'required',
                'integer',
            ],
            'marca_id' => [
                'required',
                'integer',
            ],
            'nombre' => [
                'string',
                'required',
            ],
            'precio_de_venta' => [
                'required',
            ],
            'precio_de_compra' => [
                'required',
            ],
            'composicion_de_sal' => [
                'string',
                'required',
            ],
        ];
    }
}
