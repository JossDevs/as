<?php

namespace App\Http\Requests;

use App\Models\DepartamentoMedico;
use Gate;
use Illuminate\Foundation\Http\FormRequest;
use Symfony\Component\HttpFoundation\Response;

class MassDestroyDepartamentoMedicoRequest extends FormRequest
{
    public function authorize()
    {
        abort_if(Gate::denies('departamento_medico_delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return true;
    }

    public function rules()
    {
        return [
            'ids'   => 'required|array',
            'ids.*' => 'exists:departamento_medicos,id',
        ];
    }
}
