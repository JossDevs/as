<?php

namespace App\Http\Requests;

use App\Models\Paciente;
use Gate;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Response;

class UpdatePacienteRequest extends FormRequest
{
    public function authorize()
    {
        return Gate::allows('paciente_edit');
    }

    public function rules()
    {
        return [
            'usuario_id' => [
                'required',
                'integer',
            ],
        ];
    }
}
