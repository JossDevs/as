<?php

namespace App\Http\Requests;

use App\Models\Farmacium;
use Gate;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Response;

class StoreFarmaciumRequest extends FormRequest
{
    public function authorize()
    {
        return Gate::allows('farmacium_create');
    }

    public function rules()
    {
        return [
            'usuario_id' => [
                'required',
                'integer',
            ],
        ];
    }
}
