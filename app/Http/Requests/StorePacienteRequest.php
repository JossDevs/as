<?php

namespace App\Http\Requests;

use App\Models\Paciente;
use Gate;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Response;

class StorePacienteRequest extends FormRequest
{
    public function authorize()
    {
        return Gate::allows('paciente_create');
    }

    public function rules()
    {
        return [
            'usuario_id' => [
                'required',
                'integer',
            ],
        ];
    }
}
