<?php

namespace App\Http\Requests;

use App\Models\Medico;
use Gate;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Response;

class StoreMedicoRequest extends FormRequest
{
    public function authorize()
    {
        return Gate::allows('medico_create');
    }

    public function rules()
    {
        return [
            'usuario_id' => [
                'required',
                'integer',
            ],
            'departamento_medico_id' => [
                'required',
                'integer',
            ],
            'especialista' => [
                'string',
                'required',
            ],
        ];
    }
}
