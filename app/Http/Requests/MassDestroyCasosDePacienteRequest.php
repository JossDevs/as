<?php

namespace App\Http\Requests;

use App\Models\CasosDePaciente;
use Gate;
use Illuminate\Foundation\Http\FormRequest;
use Symfony\Component\HttpFoundation\Response;

class MassDestroyCasosDePacienteRequest extends FormRequest
{
    public function authorize()
    {
        abort_if(Gate::denies('casos_de_paciente_delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return true;
    }

    public function rules()
    {
        return [
            'ids'   => 'required|array',
            'ids.*' => 'exists:casos_de_pacientes,id',
        ];
    }
}
