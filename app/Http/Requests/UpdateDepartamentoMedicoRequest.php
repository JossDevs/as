<?php

namespace App\Http\Requests;

use App\Models\DepartamentoMedico;
use Gate;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Response;

class UpdateDepartamentoMedicoRequest extends FormRequest
{
    public function authorize()
    {
        return Gate::allows('departamento_medico_edit');
    }

    public function rules()
    {
        return [
            'titulo' => [
                'string',
                'required',
            ],
        ];
    }
}
