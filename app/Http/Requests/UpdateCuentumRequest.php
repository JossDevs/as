<?php

namespace App\Http\Requests;

use App\Models\Cuentum;
use Gate;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Response;

class UpdateCuentumRequest extends FormRequest
{
    public function authorize()
    {
        return Gate::allows('cuentum_edit');
    }

    public function rules()
    {
        return [
            'paciente_id' => [
                'required',
                'integer',
            ],
            'fecha_factura' => [
                'required',
                'date_format:' . config('panel.date_format'),
            ],
            'importe' => [
                'numeric',
                'required',
            ],
        ];
    }
}
