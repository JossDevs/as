<?php

namespace App\Http\Requests;

use App\Models\Recepcionistum;
use Gate;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Response;

class StoreRecepcionistumRequest extends FormRequest
{
    public function authorize()
    {
        return Gate::allows('recepcionistum_create');
    }

    public function rules()
    {
        return [
            'usuario_id' => [
                'required',
                'integer',
            ],
        ];
    }
}
