<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\MassDestroyRecepcionistumRequest;
use App\Http\Requests\StoreRecepcionistumRequest;
use App\Http\Requests\UpdateRecepcionistumRequest;
use App\Models\Recepcionistum;
use App\Models\User;
use Gate;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class RecepcionistaController extends Controller
{
    public function index()
    {
        abort_if(Gate::denies('recepcionistum_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $recepcionista = Recepcionistum::with(['usuario', 'created'])->get();

        return view('admin.recepcionista.index', compact('recepcionista'));
    }

    public function create()
    {
        abort_if(Gate::denies('recepcionistum_create'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $usuarios = User::pluck('name', 'id')->prepend(trans('global.pleaseSelect'), '');

        return view('admin.recepcionista.create', compact('usuarios'));
    }

    public function store(StoreRecepcionistumRequest $request)
    {
        $recepcionistum = Recepcionistum::create($request->all());

        return redirect()->route('admin.recepcionista.index');
    }

    public function edit(Recepcionistum $recepcionistum)
    {
        abort_if(Gate::denies('recepcionistum_edit'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $usuarios = User::pluck('name', 'id')->prepend(trans('global.pleaseSelect'), '');

        $recepcionistum->load('usuario', 'created');

        return view('admin.recepcionista.edit', compact('usuarios', 'recepcionistum'));
    }

    public function update(UpdateRecepcionistumRequest $request, Recepcionistum $recepcionistum)
    {
        $recepcionistum->update($request->all());

        return redirect()->route('admin.recepcionista.index');
    }

    public function show(Recepcionistum $recepcionistum)
    {
        abort_if(Gate::denies('recepcionistum_show'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $recepcionistum->load('usuario', 'created');

        return view('admin.recepcionista.show', compact('recepcionistum'));
    }

    public function destroy(Recepcionistum $recepcionistum)
    {
        abort_if(Gate::denies('recepcionistum_delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $recepcionistum->delete();

        return back();
    }

    public function massDestroy(MassDestroyRecepcionistumRequest $request)
    {
        Recepcionistum::whereIn('id', request('ids'))->delete();

        return response(null, Response::HTTP_NO_CONTENT);
    }
}
