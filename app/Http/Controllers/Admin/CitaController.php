<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\MassDestroyCitumRequest;
use App\Http\Requests\StoreCitumRequest;
use App\Http\Requests\UpdateCitumRequest;
use App\Models\Citum;
use App\Models\Departamento;
use App\Models\Medico;
use App\Models\User;
use Gate;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class CitaController extends Controller
{
    public function index()
    {
        abort_if(Gate::denies('citum_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $cita = Citum::with(['paciente', 'medico', 'departamento', 'created_by'])->get();

        return view('admin.cita.index', compact('cita'));
    }

    public function create()
    {
        abort_if(Gate::denies('citum_create'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $pacientes = User::pluck('name', 'id')->prepend(trans('global.pleaseSelect'), '');

        $medicos = Medico::pluck('especialista', 'id')->prepend(trans('global.pleaseSelect'), '');

        $departamentos = Departamento::pluck('nombre', 'id')->prepend(trans('global.pleaseSelect'), '');

        return view('admin.cita.create', compact('pacientes', 'medicos', 'departamentos'));
    }

    public function store(StoreCitumRequest $request)
    {
        $citum = Citum::create($request->all());

        return redirect()->route('admin.cita.index');
    }

    public function edit(Citum $citum)
    {
        abort_if(Gate::denies('citum_edit'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $pacientes = User::pluck('name', 'id')->prepend(trans('global.pleaseSelect'), '');

        $medicos = Medico::pluck('especialista', 'id')->prepend(trans('global.pleaseSelect'), '');

        $departamentos = Departamento::pluck('nombre', 'id')->prepend(trans('global.pleaseSelect'), '');

        $citum->load('paciente', 'medico', 'departamento', 'created_by');

        return view('admin.cita.edit', compact('pacientes', 'medicos', 'departamentos', 'citum'));
    }

    public function update(UpdateCitumRequest $request, Citum $citum)
    {
        $citum->update($request->all());

        return redirect()->route('admin.cita.index');
    }

    public function show(Citum $citum)
    {
        abort_if(Gate::denies('citum_show'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $citum->load('paciente', 'medico', 'departamento', 'created_by');

        return view('admin.cita.show', compact('citum'));
    }

    public function destroy(Citum $citum)
    {
        abort_if(Gate::denies('citum_delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $citum->delete();

        return back();
    }

    public function massDestroy(MassDestroyCitumRequest $request)
    {
        Citum::whereIn('id', request('ids'))->delete();

        return response(null, Response::HTTP_NO_CONTENT);
    }
}
