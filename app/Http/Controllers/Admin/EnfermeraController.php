<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\MassDestroyEnfermeraRequest;
use App\Http\Requests\StoreEnfermeraRequest;
use App\Http\Requests\UpdateEnfermeraRequest;
use App\Models\Enfermera;
use App\Models\User;
use Gate;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class EnfermeraController extends Controller
{
    public function index()
    {
        abort_if(Gate::denies('enfermera_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $enfermeras = Enfermera::with(['usuario', 'created_by'])->get();

        return view('admin.enfermeras.index', compact('enfermeras'));
    }

    public function create()
    {
        abort_if(Gate::denies('enfermera_create'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $usuarios = User::pluck('name', 'id')->prepend(trans('global.pleaseSelect'), '');

        return view('admin.enfermeras.create', compact('usuarios'));
    }

    public function store(StoreEnfermeraRequest $request)
    {
        $enfermera = Enfermera::create($request->all());

        return redirect()->route('admin.enfermeras.index');
    }

    public function edit(Enfermera $enfermera)
    {
        abort_if(Gate::denies('enfermera_edit'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $usuarios = User::pluck('name', 'id')->prepend(trans('global.pleaseSelect'), '');

        $enfermera->load('usuario', 'created_by');

        return view('admin.enfermeras.edit', compact('usuarios', 'enfermera'));
    }

    public function update(UpdateEnfermeraRequest $request, Enfermera $enfermera)
    {
        $enfermera->update($request->all());

        return redirect()->route('admin.enfermeras.index');
    }

    public function show(Enfermera $enfermera)
    {
        abort_if(Gate::denies('enfermera_show'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $enfermera->load('usuario', 'created_by');

        return view('admin.enfermeras.show', compact('enfermera'));
    }

    public function destroy(Enfermera $enfermera)
    {
        abort_if(Gate::denies('enfermera_delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $enfermera->delete();

        return back();
    }

    public function massDestroy(MassDestroyEnfermeraRequest $request)
    {
        Enfermera::whereIn('id', request('ids'))->delete();

        return response(null, Response::HTTP_NO_CONTENT);
    }
}
