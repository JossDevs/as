<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\MassDestroyFarmaciumRequest;
use App\Http\Requests\StoreFarmaciumRequest;
use App\Http\Requests\UpdateFarmaciumRequest;
use App\Models\Farmacium;
use App\Models\User;
use Gate;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class FarmaciaController extends Controller
{
    public function index()
    {
        abort_if(Gate::denies('farmacium_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $farmacia = Farmacium::with(['usuario'])->get();

        return view('admin.farmacia.index', compact('farmacia'));
    }

    public function create()
    {
        abort_if(Gate::denies('farmacium_create'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $usuarios = User::pluck('name', 'id')->prepend(trans('global.pleaseSelect'), '');

        return view('admin.farmacia.create', compact('usuarios'));
    }

    public function store(StoreFarmaciumRequest $request)
    {
        $farmacium = Farmacium::create($request->all());

        return redirect()->route('admin.farmacia.index');
    }

    public function edit(Farmacium $farmacium)
    {
        abort_if(Gate::denies('farmacium_edit'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $usuarios = User::pluck('name', 'id')->prepend(trans('global.pleaseSelect'), '');

        $farmacium->load('usuario');

        return view('admin.farmacia.edit', compact('usuarios', 'farmacium'));
    }

    public function update(UpdateFarmaciumRequest $request, Farmacium $farmacium)
    {
        $farmacium->update($request->all());

        return redirect()->route('admin.farmacia.index');
    }

    public function show(Farmacium $farmacium)
    {
        abort_if(Gate::denies('farmacium_show'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $farmacium->load('usuario');

        return view('admin.farmacia.show', compact('farmacium'));
    }

    public function destroy(Farmacium $farmacium)
    {
        abort_if(Gate::denies('farmacium_delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $farmacium->delete();

        return back();
    }

    public function massDestroy(MassDestroyFarmaciumRequest $request)
    {
        Farmacium::whereIn('id', request('ids'))->delete();

        return response(null, Response::HTTP_NO_CONTENT);
    }
}
