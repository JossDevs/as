<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\MassDestroyMarcaRequest;
use App\Http\Requests\StoreMarcaRequest;
use App\Http\Requests\UpdateMarcaRequest;
use App\Models\Marca;
use Gate;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class MarcaController extends Controller
{
    public function index()
    {
        abort_if(Gate::denies('marca_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $marcas = Marca::with(['created_by'])->get();

        return view('admin.marcas.index', compact('marcas'));
    }

    public function create()
    {
        abort_if(Gate::denies('marca_create'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return view('admin.marcas.create');
    }

    public function store(StoreMarcaRequest $request)
    {
        $marca = Marca::create($request->all());

        return redirect()->route('admin.marcas.index');
    }

    public function edit(Marca $marca)
    {
        abort_if(Gate::denies('marca_edit'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $marca->load('created_by');

        return view('admin.marcas.edit', compact('marca'));
    }

    public function update(UpdateMarcaRequest $request, Marca $marca)
    {
        $marca->update($request->all());

        return redirect()->route('admin.marcas.index');
    }

    public function show(Marca $marca)
    {
        abort_if(Gate::denies('marca_show'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $marca->load('created_by', 'marcaMedicamentos');

        return view('admin.marcas.show', compact('marca'));
    }

    public function destroy(Marca $marca)
    {
        abort_if(Gate::denies('marca_delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $marca->delete();

        return back();
    }

    public function massDestroy(MassDestroyMarcaRequest $request)
    {
        Marca::whereIn('id', request('ids'))->delete();

        return response(null, Response::HTTP_NO_CONTENT);
    }
}
