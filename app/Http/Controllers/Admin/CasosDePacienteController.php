<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\MassDestroyCasosDePacienteRequest;
use App\Http\Requests\StoreCasosDePacienteRequest;
use App\Http\Requests\UpdateCasosDePacienteRequest;
use App\Models\CasosDePaciente;
use App\Models\Medico;
use App\Models\User;
use Gate;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class CasosDePacienteController extends Controller
{
    public function index()
    {
        abort_if(Gate::denies('casos_de_paciente_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $casosDePacientes = CasosDePaciente::with(['medico', 'paciente'])->get();

        return view('admin.casosDePacientes.index', compact('casosDePacientes'));
    }

    public function create()
    {
        abort_if(Gate::denies('casos_de_paciente_create'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $medicos = Medico::pluck('especialista', 'id')->prepend(trans('global.pleaseSelect'), '');

        $pacientes = User::pluck('name', 'id')->prepend(trans('global.pleaseSelect'), '');

        return view('admin.casosDePacientes.create', compact('medicos', 'pacientes'));
    }

    public function store(StoreCasosDePacienteRequest $request)
    {
        $casosDePaciente = CasosDePaciente::create($request->all());

        return redirect()->route('admin.casos-de-pacientes.index');
    }

    public function edit(CasosDePaciente $casosDePaciente)
    {
        abort_if(Gate::denies('casos_de_paciente_edit'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $medicos = Medico::pluck('especialista', 'id')->prepend(trans('global.pleaseSelect'), '');

        $pacientes = User::pluck('name', 'id')->prepend(trans('global.pleaseSelect'), '');

        $casosDePaciente->load('medico', 'paciente');

        return view('admin.casosDePacientes.edit', compact('medicos', 'pacientes', 'casosDePaciente'));
    }

    public function update(UpdateCasosDePacienteRequest $request, CasosDePaciente $casosDePaciente)
    {
        $casosDePaciente->update($request->all());

        return redirect()->route('admin.casos-de-pacientes.index');
    }

    public function show(CasosDePaciente $casosDePaciente)
    {
        abort_if(Gate::denies('casos_de_paciente_show'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $casosDePaciente->load('medico', 'paciente');

        return view('admin.casosDePacientes.show', compact('casosDePaciente'));
    }

    public function destroy(CasosDePaciente $casosDePaciente)
    {
        abort_if(Gate::denies('casos_de_paciente_delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $casosDePaciente->delete();

        return back();
    }

    public function massDestroy(MassDestroyCasosDePacienteRequest $request)
    {
        CasosDePaciente::whereIn('id', request('ids'))->delete();

        return response(null, Response::HTTP_NO_CONTENT);
    }
}
