<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\MassDestroyCuentumRequest;
use App\Http\Requests\StoreCuentumRequest;
use App\Http\Requests\UpdateCuentumRequest;
use App\Models\Cuentum;
use App\Models\User;
use Gate;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class CuentaController extends Controller
{
    public function index()
    {
        abort_if(Gate::denies('cuentum_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $cuenta = Cuentum::with(['paciente', 'created_by'])->get();

        return view('admin.cuenta.index', compact('cuenta'));
    }

    public function create()
    {
        abort_if(Gate::denies('cuentum_create'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $pacientes = User::pluck('name', 'id')->prepend(trans('global.pleaseSelect'), '');

        return view('admin.cuenta.create', compact('pacientes'));
    }

    public function store(StoreCuentumRequest $request)
    {
        $cuentum = Cuentum::create($request->all());

        return redirect()->route('admin.cuenta.index');
    }

    public function edit(Cuentum $cuentum)
    {
        abort_if(Gate::denies('cuentum_edit'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $pacientes = User::pluck('name', 'id')->prepend(trans('global.pleaseSelect'), '');

        $cuentum->load('paciente', 'created_by');

        return view('admin.cuenta.edit', compact('pacientes', 'cuentum'));
    }

    public function update(UpdateCuentumRequest $request, Cuentum $cuentum)
    {
        $cuentum->update($request->all());

        return redirect()->route('admin.cuenta.index');
    }

    public function show(Cuentum $cuentum)
    {
        abort_if(Gate::denies('cuentum_show'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $cuentum->load('paciente', 'created_by');

        return view('admin.cuenta.show', compact('cuentum'));
    }

    public function destroy(Cuentum $cuentum)
    {
        abort_if(Gate::denies('cuentum_delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $cuentum->delete();

        return back();
    }

    public function massDestroy(MassDestroyCuentumRequest $request)
    {
        Cuentum::whereIn('id', request('ids'))->delete();

        return response(null, Response::HTTP_NO_CONTENT);
    }
}
