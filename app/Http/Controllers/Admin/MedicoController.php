<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\MassDestroyMedicoRequest;
use App\Http\Requests\StoreMedicoRequest;
use App\Http\Requests\UpdateMedicoRequest;
use App\Models\DepartamentoMedico;
use App\Models\Medico;
use App\Models\User;
use Gate;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class MedicoController extends Controller
{
    public function index()
    {
        abort_if(Gate::denies('medico_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $medicos = Medico::with(['usuario', 'departamento_medico', 'created_by'])->get();

        return view('admin.medicos.index', compact('medicos'));
    }

    public function create()
    {
        abort_if(Gate::denies('medico_create'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $usuarios = User::pluck('name', 'id')->prepend(trans('global.pleaseSelect'), '');

        $departamento_medicos = DepartamentoMedico::pluck('titulo', 'id')->prepend(trans('global.pleaseSelect'), '');

        return view('admin.medicos.create', compact('usuarios', 'departamento_medicos'));
    }

    public function store(StoreMedicoRequest $request)
    {
        $medico = Medico::create($request->all());

        return redirect()->route('admin.medicos.index');
    }

    public function edit(Medico $medico)
    {
        abort_if(Gate::denies('medico_edit'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $usuarios = User::pluck('name', 'id')->prepend(trans('global.pleaseSelect'), '');

        $departamento_medicos = DepartamentoMedico::pluck('titulo', 'id')->prepend(trans('global.pleaseSelect'), '');

        $medico->load('usuario', 'departamento_medico', 'created_by');

        return view('admin.medicos.edit', compact('usuarios', 'departamento_medicos', 'medico'));
    }

    public function update(UpdateMedicoRequest $request, Medico $medico)
    {
        $medico->update($request->all());

        return redirect()->route('admin.medicos.index');
    }

    public function show(Medico $medico)
    {
        abort_if(Gate::denies('medico_show'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $medico->load('usuario', 'departamento_medico', 'created_by', 'medicoCita');

        return view('admin.medicos.show', compact('medico'));
    }

    public function destroy(Medico $medico)
    {
        abort_if(Gate::denies('medico_delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $medico->delete();

        return back();
    }

    public function massDestroy(MassDestroyMedicoRequest $request)
    {
        Medico::whereIn('id', request('ids'))->delete();

        return response(null, Response::HTTP_NO_CONTENT);
    }
}
