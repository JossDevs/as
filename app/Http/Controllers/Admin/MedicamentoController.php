<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\MassDestroyMedicamentoRequest;
use App\Http\Requests\StoreMedicamentoRequest;
use App\Http\Requests\UpdateMedicamentoRequest;
use App\Models\Categorium;
use App\Models\Marca;
use App\Models\Medicamento;
use Gate;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class MedicamentoController extends Controller
{
    public function index()
    {
        abort_if(Gate::denies('medicamento_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $medicamentos = Medicamento::with(['categoria', 'marca', 'created_by'])->get();

        return view('admin.medicamentos.index', compact('medicamentos'));
    }

    public function create()
    {
        abort_if(Gate::denies('medicamento_create'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $categorias = Categorium::pluck('nombre', 'id')->prepend(trans('global.pleaseSelect'), '');

        $marcas = Marca::pluck('nombre', 'id')->prepend(trans('global.pleaseSelect'), '');

        return view('admin.medicamentos.create', compact('categorias', 'marcas'));
    }

    public function store(StoreMedicamentoRequest $request)
    {
        $medicamento = Medicamento::create($request->all());

        return redirect()->route('admin.medicamentos.index');
    }

    public function edit(Medicamento $medicamento)
    {
        abort_if(Gate::denies('medicamento_edit'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $categorias = Categorium::pluck('nombre', 'id')->prepend(trans('global.pleaseSelect'), '');

        $marcas = Marca::pluck('nombre', 'id')->prepend(trans('global.pleaseSelect'), '');

        $medicamento->load('categoria', 'marca', 'created_by');

        return view('admin.medicamentos.edit', compact('categorias', 'marcas', 'medicamento'));
    }

    public function update(UpdateMedicamentoRequest $request, Medicamento $medicamento)
    {
        $medicamento->update($request->all());

        return redirect()->route('admin.medicamentos.index');
    }

    public function show(Medicamento $medicamento)
    {
        abort_if(Gate::denies('medicamento_show'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $medicamento->load('categoria', 'marca', 'created_by');

        return view('admin.medicamentos.show', compact('medicamento'));
    }

    public function destroy(Medicamento $medicamento)
    {
        abort_if(Gate::denies('medicamento_delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $medicamento->delete();

        return back();
    }

    public function massDestroy(MassDestroyMedicamentoRequest $request)
    {
        Medicamento::whereIn('id', request('ids'))->delete();

        return response(null, Response::HTTP_NO_CONTENT);
    }
}
