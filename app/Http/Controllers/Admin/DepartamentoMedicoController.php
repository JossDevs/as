<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\MassDestroyDepartamentoMedicoRequest;
use App\Http\Requests\StoreDepartamentoMedicoRequest;
use App\Http\Requests\UpdateDepartamentoMedicoRequest;
use App\Models\DepartamentoMedico;
use Gate;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class DepartamentoMedicoController extends Controller
{
    public function index()
    {
        abort_if(Gate::denies('departamento_medico_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $departamentoMedicos = DepartamentoMedico::with(['created_by'])->get();

        return view('admin.departamentoMedicos.index', compact('departamentoMedicos'));
    }

    public function create()
    {
        abort_if(Gate::denies('departamento_medico_create'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return view('admin.departamentoMedicos.create');
    }

    public function store(StoreDepartamentoMedicoRequest $request)
    {
        $departamentoMedico = DepartamentoMedico::create($request->all());

        return redirect()->route('admin.departamento-medicos.index');
    }

    public function edit(DepartamentoMedico $departamentoMedico)
    {
        abort_if(Gate::denies('departamento_medico_edit'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $departamentoMedico->load('created_by');

        return view('admin.departamentoMedicos.edit', compact('departamentoMedico'));
    }

    public function update(UpdateDepartamentoMedicoRequest $request, DepartamentoMedico $departamentoMedico)
    {
        $departamentoMedico->update($request->all());

        return redirect()->route('admin.departamento-medicos.index');
    }

    public function show(DepartamentoMedico $departamentoMedico)
    {
        abort_if(Gate::denies('departamento_medico_show'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $departamentoMedico->load('created_by', 'departamentoMedicoMedicos');

        return view('admin.departamentoMedicos.show', compact('departamentoMedico'));
    }

    public function destroy(DepartamentoMedico $departamentoMedico)
    {
        abort_if(Gate::denies('departamento_medico_delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $departamentoMedico->delete();

        return back();
    }

    public function massDestroy(MassDestroyDepartamentoMedicoRequest $request)
    {
        DepartamentoMedico::whereIn('id', request('ids'))->delete();

        return response(null, Response::HTTP_NO_CONTENT);
    }
}
